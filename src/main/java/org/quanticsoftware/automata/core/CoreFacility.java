package org.quanticsoftware.automata.core;

import org.quanticsoftware.automata.runtime.CauldronFacility;

public class CoreFacility extends CauldronFacility {

	public CoreFacility() {
		super("core");
		
		var function = function("add");
		function.input("double", "a");
		function.input("double", "b");
		function.output("double");
		function.rule((s)->s.output().set(s.getd("a") + s.getd("b")));
		
		function = function("sub");
		function.input("double", "a");
		function.input("double", "b");
		function.output("double");
		function.rule((s)->s.output().set(s.getd("a") - s.getd("b")));
		
		function = function("mul");
		function.input("double", "a");
		function.input("double", "b");
		function.output("double");
		function.rule((s)->s.output().set(s.getd("a") * s.getd("b")));

		function = function("div");
		function.input("double", "a");
		function.input("double", "b");
		function.output("double");
		function.rule((s)->s.output().set(s.getd("a") / s.getd("b")));
	}

}
