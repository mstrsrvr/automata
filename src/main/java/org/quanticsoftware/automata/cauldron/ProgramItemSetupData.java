package org.quanticsoftware.automata.cauldron;

import java.util.Map;

public class ProgramItemSetupData {
    public BuildItem reference;
    public Map<String, ProgramItemInput> input;
    public ObjectConnection oconn;
    public BuildData bdata;
    public BuildSpec spec;
    public boolean dataonly;
    
    public ProgramItemSetupData(BuildItem reference, ObjectConnection oconn) {
        this.reference = reference;
        this.oconn = oconn;
    }
}

