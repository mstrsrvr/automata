package org.quanticsoftware.automata.cauldron;

import java.util.Map;

public class CauldronProgramContext {
	public String name, target;
	public ProgramItem[] items;
	public Map<String, String> input;
	public BuildContext buildctx;
}
