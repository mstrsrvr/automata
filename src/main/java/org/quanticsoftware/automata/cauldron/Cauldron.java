package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.application.ContextEntry;
import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.runtime.DatabaseRuleSession;
import org.quanticsoftware.automata.runtime.ObjectEqualsRule;
import org.quanticsoftware.automata.runtime.Runtime;
import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.runtime.RuntimeInit;
import org.quanticsoftware.automata.runtime.Target;
import org.quanticsoftware.automata.runtime.TargetRule;
import org.quanticsoftware.automata.runtime.Test;

public class Cauldron {
	private static final boolean ALLOW_DUPS = true;
	private static final boolean DISALLOW_DUPS = false;
	private TargetRule objecteq;
	
	public Cauldron() {
		objecteq = new ObjectEqualsRule();
	}
	
	private final void addFunction(
			BuildSpec spec,
			String facname,
			String fncname,
			Map<String, String> input,
			String output,
			boolean template,
			boolean passthrough,
			int blockmode) {
		
		var facility = spec.facilities.get(facname);
		if (facility == null)
			spec.facilities.put(facname, facility = new HashMap<>());
		
		facility.put(fncname, new SpecFunction(
				input,
				output,
				template,
				passthrough,
				blockmode));
	}
	
	private final TestProgram assemblyProgram(BuildSpec spec, BuildData bdata) {
		var program = program(
				spec,
				spec.buildctx.target.getName(),
				bdata.program);
		
		return new TestProgram(program, spec);
	}
	
	private final void assignTemplateSignature(
			BuildSpec spec,
			String signature,
			String facility,
			String function) {
		spec.templates.get(signature).add(new String[] {facility, function});
	}
	
	public final Map<String, CauldronProgram> build(BuildContext buildctx) {
		TestProgram draft;
		
		var spec = specsetup(buildctx);
		var upgrade = false;
		var ret = false;
		
		while (!ret) {
			try {
				draft = generate(spec, upgrade);
			} catch (TerminateException e) {
				break;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			
			if (draft == null) {
				upgrade = true;
				continue;
			}

			buildtargets(draft);
			
			upgrade = false;
			if (draft.program.context.items.length == 0) {
				ret = !testContinue(draft);
				continue;
			}
			
			var tests = shuffle(draft);
			for (TestProgram test : tests)
				if ((ret = !testContinue(test)))
					break;
		}
		
		return (spec.resultc == 0)? null : spec.validated;
	}
	
	private final void buildfunctionsconns(BuildSpec spec) {
		var facilities = spec.buildctx.rtctx.datactx.get("context.facilities");
		for (var fackey : facilities.keySet()) {
			var facility = facilities.getItem(fackey);
			var functions = facility.getItem("functions");
			for (var fnckey : functions.keySet()) {
				var function = functions.getItem(fnckey);
				functionconnsbuild(spec, (String)fackey, function);
			}
		}
	}
	
	private final void buildinputconns(BuildSpec spec) {
		for (var input : spec.test.datactx.entries()) {
			var object = spec.test.datactx.get(input);
			spec.ctxpool.instance(input, object);
		}
	}
	
	private final CauldronProgram buildondemand(
			BuildContext buildctx,
			Map<String, CauldronProgram> programs,
			ProgramItemInput args) {
		
		var nbuildctx = new BuildContext();
		nbuildctx.resultlimit = 1;
		nbuildctx.rtctx = buildctx.rtctx;
		nbuildctx.rtinit = buildctx.rtinit;
		
		var type = buildctx.rtctx.typectx().get(args.type);
		nbuildctx.target = new Target(type, args.program);
		nbuildctx.target.input.putAll(buildctx.target.input);
		
		for (var test : buildctx.target.tests) {
			var ntest = nbuildctx.target.test();
			for (var key : buildctx.target.input.keySet())
				ntest.input(key, test.datactx.get(key));
			
			var itest = test.titems.get(args.input);
			ntest.rule = itest.rule;
			for (var key : itest.titems.keySet())
				ntest.instance(key).rule = itest.titems.get(key).rule;
		}
		
		programs.putAll(build(nbuildctx));
		
		var program = programs.get(args.program);
		program.context.buildctx = nbuildctx;
		
		return program;
	}
	
	private final void buildtargets(TestProgram testp) {
		for (var item : testp.program.context.items)
			for (var ikey : item.input.keySet()) {
				var args = item.input.get(ikey);
				if (args.program == null)
					continue;
				
				if (testp.spec.validated.containsKey(args.program))
					continue;
				
				buildondemand(
						testp.spec.buildctx,
						testp.spec.validated,
						args);
			}
	}
	
	private final void checkReqs(
			RuntimeContext rtctx,
			Runtime runtime,
			Target target,
			RuntimeInit rtinit) {
		for (var key : target.testonly)
			runtime.build(this, rtctx.targets.get(key), rtinit);
	}
	
	private final BuildItem clone(BuildItem reference) {
		var bitem = new BuildItem();
		bitem.pitem = reference.pitem;
		return bitem;
	}
	
	private static final ProgramItem clone(
			BuildSpec spec,
			BuildData bdata,
			Map<String, ProgramItemInput> input,
			ProgramItem item,
			Map<String, String> pinputs) {
		
		if (item.dontclone)
			return item;
		
		var nitem = new ProgramItem(
				item.facility,
				item.function,
				item.reference,
				item.index);
		
		nitem.source = item.source;
		for (var ikey : item.input.keySet()) {
			if (isFunction(item, ikey) || (bdata == null)) {
			    nitem.input.put(ikey, item.input.get(ikey));
			    continue;
			}
			
			var type = item.input.get(ikey).type;
			var args = input.get(ikey);
			if (pinputs.containsKey(ikey))
			    continue;
			
			var value = spec.test.datactx.get(args.object);
			if (!value.getType().isCompatible(type))
			    continue;
			
			nitem.input.put(ikey, args);
			pinputs.put(ikey, type);
		}
		
		return nitem;
	}
	
	private final ContextPool cloneContextPool(BuildSpec spec) {
		var dctxpool = new ContextPool();
		for (var okey : spec.test.datactx.entries())
		    copy(spec, dctxpool, okey);
		return dctxpool;
	}
	
	private final String[] convertInput(Map<String, ProgramItemInput> inputs) {
		int i = 0;
		
		if (inputs == null)
		    return new String[0];
		
		var input = new String[inputs.size()];
		for (var key : inputs.keySet())
		    input[i++] = inputs.get(key).object;
		
		return input;
	}
	
	private final String copy(
			BuildSpec spec,
			ContextPool ctxpool,
			String from) {
		
		var ofrom = spec.test.datactx.get(from);
		var params = new InstanceParameters();
		params.iname = from;
		params.object = ofrom;
		
		return ctxpool.instance(params);
	}
	
	private final BuildData copyShuffledItems(
	        BuildSpec spec,
	        ProgramItem[] items,
	        Map<String, ProgramItemInput> input) {
		
		var bdata = new BuildData();
		for (var item : items)
			bdata.program.push(clone(
					spec,
					bdata,
					input,
					item,
					new HashMap<>())
			);
		
		return bdata;
	}
	
	private final Map<String, String> finputconnect(
			BuildSpec spec,
			String facname,
			String fncname,
			Map<String, String> input) {
		
		var tinput = new HashMap<String, String>();
		for (var key : input.keySet()) {
			var type = input.get(key);
			spec.ctxpool.connect(facname, fncname, "input", key, type);
			tinput.put(key, type);
		}
		
		return tinput;
	}
	
	private final void functionconnsbuild(
			BuildSpec spec,
			String fackey,
			DataObject function) {
		
		var fnckey = function.getst("name");
		var input = getInput(function);
		finputconnect(spec, fackey, fnckey, input);
		
		var otype = function.getst("output");
		spec.ctxpool.connect(fackey, fnckey, "output", "output", otype);
		var passthrough = function.getbl("passthrough");
		var blockmode = function.geti("block_mode");
		addFunction(
				spec,
				fackey,
				fnckey,
				input,
				otype,
				false,
				passthrough,
				blockmode);
		
		if (passthrough)
			return;
		
		var signature = getFunctionSignature(input, otype, blockmode);
		if (!isTemplateRegistered(spec, signature))
			registerTemplate(signature, spec, fackey, input, otype, blockmode);
		
		assignTemplateSignature(spec, signature, fackey, fnckey);
	}
	
	private final TestProgram generate(BuildSpec spec, boolean upgrade)
			throws Exception {
		var tpath = spec.buildctx.target.getType().getPath();
		var oconns = spec.ctxpool.getConnections(tpath);
		
		var bdata = new BuildData();
		var noproc = true;
		
		if (upgrade && spec.clevel > -1)
			spec.clevel = (spec.levellimit)? spec.clevel + 1 : spec.clevel;
		
		spec.levellimit = false;
		spec.programid = spec.clevel * 10000;
		for (var oconn : oconns) {
			if (oconn.facility == null)
				continue;
			
			if ((oconn.direction != null) && !oconn.direction.equals("output"))
				continue;
			
			noproc = false;
			bdata.levellimit = 0;
			bdata.oconn = oconn;
			bdata.reference = clone(spec.reference);
			bdata.source = null;
			bdata.nextblockmode = 0;
			bdata.program.clear();
			spec.consumed.clear();
			spec.pinputs.clear();
			
			try {
				programitembuild(spec, bdata);
			} catch (AssemblySuccessException eas) {
				spec.branches.add(eas.branch);
				return assemblyProgram(spec, bdata);
			}
		}

		if (noproc || !spec.levellimit)
			throw new TerminateException();
		
		return null;
	}
	
	private final StringBuilder getBranch(List<ProgramItem> program) {
		StringBuilder branch = null;
		for (var item : program) {
		    if (branch == null)
		        branch = new StringBuilder("[");
		    else
		        branch.append(".[");
		    branch.append(item.toString()).append("]");
		}
		return branch;
	}
	
	private final String getBranch(
	        ObjectConnection oconn,
	        String[] input,
	        List<ProgramItem> program) {
		StringBuilder branch;
		if (oconn == null)
		    return null;
		if ((branch = getBranch(program)) == null)
		    branch = new StringBuilder("[");
		else
		    branch.append(".[");
		return branch.append(
		        getCurrentExpression(oconn, input)).append("]").toString();
	}
	
	private final List<Map<String, ProgramItemInput>> getCompatibleInput(
	        BuildSpec spec,
	        BuildData bdata) {
		var specfnc = spec.facilities.
				get(bdata.oconn.facility).
				get(bdata.oconn.function);
		
		if (specfnc.input.size() == 0)
		    return null;
		
		return getCompatibleInput(
				spec,
				spec.test,
				spec.ctxpool,
				bdata.oconn,
				bdata.reference,
				specfnc,
				DISALLOW_DUPS);
	}
	
	private final List<Map<String, ProgramItemInput>> getCompatibleInput(
			BuildSpec spec,
			Test test,
			ContextPool ctxpool,
			ObjectConnection oconn,
			BuildItem bitem,
			SpecFunction specfnc,
			boolean allowdups) {
		
		Map<String, ProgramItemInput> sinput;
		List<Map<String, ProgramItemInput>> sinputs;
		int fisize;

		var largs = getCompatibleParameters(
				spec,
				test,
				ctxpool,
				oconn,
				bitem,
				specfnc);
		
		if ((largs == null) || largs.size() == 0)
		    return null;
		
		fisize = specfnc.input.size();
		var lines = (int)Math.pow(2, largs.size());
		sinputs = null;
		
		var dup = new HashSet<String>();
		for (int i = 0; i < lines; i++) {
			var c = 0;
			sinput = null;
			dup.clear();
			
			for (var args : largs) {
				c = (c == 0)? 1 : c << 1;
				if ((i & c) == 0)
				    continue;
				
				if ((sinput != null) && sinput.containsKey(args.input)) {
				    sinput = null;
				    break;
				}
				
				if (sinput == null)
				    sinput = new HashMap<>();
				
				if (isDuplicated(dup, args)) {
				    sinput.clear();
				    sinput = null;
				    break;
				}
				
				if (!allowdups) {
					dup.add(args.object);
					if (!hasValidNameSpace(test.datactx, sinput, args)) {
						sinput = null;
						break;
					}
				}
				
				sinput.put(args.input, args);
			}
			
			if ((sinput == null) || (sinput.size() != fisize))
			    continue;
			
			if (sinputs == null)
			    sinputs = new LinkedList<>();
			
			sinputs.add(sinput);
		}

		return sinputs;
	}

	private final List<ProgramItemInput> getCompatibleParameters(
			BuildSpec spec,
			Test test,
			ContextPool ctxpool,
			ObjectConnection oconn,
			BuildItem bitem,
			SpecFunction specfnc) {
		String[] fsource;
		List<ProgramItemInput> largs = null;
		
		for (var fikey : specfnc.input.keySet()) {
			var finame = GetParameterName.run(
					oconn.facility,
					oconn.function,
					fikey);
			
			var inputs = getCompatibleSources(ctxpool, finame);
			if (inputs == null)
			    continue;
			
			var type = specfnc.input.get(fikey);
			for (var iconn : inputs) {
				if (iconn.name.equals(finame))
				    continue;
				
				/*
				 * only go forward if there's a compatible data object or
				 * function output
				 */
				fsource = getFunctionSource(iconn);
				var input = test.datactx.get(iconn.name);
				if ((input == null) && (fsource == null))
				    continue;
				
				/*
				 * allows only template functions. Real function allowed only
				 * on the first level.
				 */
				if ((fsource != null) &&
				        !iconn.direction.equals("template-output"))
				    continue;
				
				/*
				 * allows only dataobjects whe parameter is function output and
				 * the target function is passthrough enabled.
				 */
				if (specfnc.passthrough && (fsource != null))
					continue;
				
				/*
				 * allows initialized only data objects
				 */
				if ((input != null) && !input.isInitialized())
					continue;
				
				if (largs == null)
				    largs = new LinkedList<>();
				
				var args = new ProgramItemInput();
				args.input = fikey;
				args.object = iconn.name;
				args.type = type;
				if (fsource != null) {
					args.facility = fsource[0];
					args.function = fsource[1];
					args.blockmode = spec.facilities.
							get(args.facility).
							get(args.function).blockmode;
				}
				largs.add(args);
			}
		}
		
		return largs;
	}
	
	private final Set<ObjectConnection> getCompatibleSources(
	        ContextPool ctxpool,
	        String target) {
		var oconn = getTypeByAbsoluteName(ctxpool, target);
		if (oconn == null)
			return null;
		
		return getSourcesByType(ctxpool, oconn.parameter);
	}
	
	private final String getCurrentExpression(
	        ObjectConnection oconn,
	        String[] input) {
		var branch = new StringBuilder();
		
		if (oconn.facility == null)
		    branch.append(oconn.name);
		else
		    branch.append(oconn.facility).
		    		append(".").
		    		append(oconn.function).
		            append("(");
		
		for (int i = 0; i < input.length; i++) {
		    if (i > 0)
		        branch.append(",");
		    branch.append(input[i]);
		}
		
		if (oconn.facility != null)
		    branch.append(")");
		
		return branch.toString();
	}
	
	private final String getExpression(
	        ProgramItemSetupData pisetup,
	        List<ProgramItem> program) {
		var input = convertInput(pisetup.input);
		return getBranch(pisetup.oconn, input, program);
	}
	
	private final String getFunctionSignature(
			Map<String, String> input,
			String output,
			int blockmode) {
		
		var sb = new StringBuilder("input: ").append(input.size());
		if (output != null)
		    sb.append(" output: ").append(output.toString());
		
		if (blockmode != 0)
			sb.append((blockmode > 0)? " begin_block" : " end_block");
		
		return sb.toString();
	}
	
	private final String[] getFunctionSource(ObjectConnection conn) {
		return (conn.direction == null)?
		        null : (conn.direction.equals("input")? null :
		        	new String[] {conn.facility, conn.function});
	}
	
	private final Map<String, String> getInput(DataObject function) {
		var map = new HashMap<String, String>();
		
		var finput = function.getItem("input");
		for (var ikey : finput.getItems()) {
			var input = finput.getItem(ikey);
			map.put(input.getst("name"), input.getst("type"));
		}
		
		return map;
	}
	
	private final Map<String, String> getInput(
			BuildSpec spec,
			ObjectConnection oconn) {
		return spec.facilities.get(oconn.facility).get(oconn.function).input;
	}
	
	private final Set<ObjectConnection> getSourcesByType(
	        ContextPool ctxpool,
	        String type) {
	    return ctxpool.getConnections(type);
	}
	
	private final ObjectConnection getTypeByAbsoluteName(
	        ContextPool ctxpool,
	        String name) {
		return ctxpool.connectionInstance(name);
	}
	
	private final boolean hasValidNameSpace(
			DataContext datactx,
			Map<String, ProgramItemInput> sinput,
			ProgramItemInput args) {
		
		if (args.function != null)
			return true;
		
		var ins = datactx.get(args.object).getNS();
		for (var tkey : sinput.keySet()) {
			var targs = sinput.get(tkey);
			if (targs.function != null)
				continue;
			
			var tobject = datactx.get(targs.object);
			var tns = tobject.getNS();
			if (!isNSCompatible(tns, ins, args.type))
				return false;
		}
		
		return true;
	}
	
	private final boolean isApproved(
			Map<String, CauldronProgram> validated,
			TestProgram testprogram) {
		
		var runtime = new Runtime();
		runtime.noPageCheck();
		runtime.dontBuildPrograms();
		runtime.useDummyConnector();
		runtime.init(testprogram.spec.buildctx.rtinit);
		var ctxentry = new ContextEntry();
		runtime.setConnector(ctxentry);
		var rtctx = runtime.context();
		
		checkReqs(
				rtctx, runtime,
				testprogram.spec.buildctx.target,
				testprogram.spec.buildctx.rtinit);
		
		for (var test : testprogram.spec.buildctx.target.tests) {
			try {
				if (test.database != null) {
					var session = new DatabaseRuleSession();
					session.datactx = new DataContext();
					session.dbsession = ctxentry.dbsession;
					ctxentry.dbsession.start();
					
					try {
						test.database.execute(session);
						ctxentry.dbsession.commit();
					} catch (Exception e) {
						ctxentry.dbsession.rollback();
						throw e;
					}
				}
				
				ctxentry.dbsession.start();
				var document = runtime.run(
						ctxentry.dbsession,
						testprogram.program,
						validated,
						test.datactx);
				
				ctxentry.dbsession.commit();
				
				if (!isApproved(test, document))
					return false;
			} catch (Exception e) {
				if (ctxentry.dbsession.isTransactionStarted())
					ctxentry.dbsession.rollback();
				
				return false;
			} finally {
				ctxentry.dbsession.close();
			}
		}
		
		return true;
	}
	
	private final boolean isApproved(Test test, DataObject object) {
		var rule = (test.rule == null)? objecteq : test.rule;
		if (!rule.isValid(test, object))
			return false;
		
		var collection = test.getType().isCollection();
		for (var key : test.titems.keySet()) {
			var target = test.titems.get(key);
			if (!collection) {
				if (!isApproved(target, object.getItem(key)))
					return false;
				continue;
			}
			
			for (var ikey : object.getItems()) {
				var item = object.getItem(ikey);
				if (!isApproved(target, item))
					return false;
			}
		}
		return true;
	}
	
	private final boolean isDataObjectOnly(Map<String, ProgramItemInput> input)
	{
		for (var ikey : input.keySet())
		    if (input.get(ikey).function != null)
		        return false;
		return true;
	}
 	
	private final boolean isDuplicated(Set<String> dup, ProgramItemInput args) {
		return (args.function != null)? false : dup.contains(args.object);
	}
	
	private final boolean isEarlierBranch(BuildSpec spec, String expression) {
		return spec.branches.contains(expression);
	}
	
	private static final boolean isFunction(ProgramItem item, String key) {
		if (!item.input.containsKey(key))
			return false;
		return item.input.get(key).function != null;
	}
	
	private final boolean isInputStarving(ProgramItemSetupData pisetup) {
		var args = new HashMap<String, int[]>();
		var oconns = new HashSet<ObjectConnection>();
		var finput = getInput(pisetup.spec, pisetup.oconn);
		for (var key : finput.keySet()) {
			var typename = finput.get(key);
			var arg = args.get(typename);
			if (arg != null) {
				arg[0]++;
				continue;
			}
			
			args.put(typename, arg = new int[2]);
			oconns.clear();
			oconns.addAll(pisetup.spec.ctxpool.cpool.get(typename));
			if (oconns.size() == 0)
				return true;
			
			for (var oconn : oconns) {
				if (oconn.facility != null)
					continue;
				
				if (!pisetup.spec.consumed.contains(oconn.name))
					arg[1]++;
			}
			
			arg[0]++;
		}
		
		for (var key : args.keySet()) {
			var arg = args.get(key);
			if (arg[0] > arg[1])
				return true;
		}
		
		return false;
	}
	
	private final boolean isNSCompatible(
			Map<String, String> tns,
			Map<String, String> ins,
			String type) {
		
		for (var key : tns.keySet()) {
			if (type.equals(key))
				continue;
			
			var ns = ins.get(key);
			if ((ns != null) && !ns.equals(tns.get(key)))
				return false;
		}
		
		return true;
	}
	
	private final boolean isTemplateRegistered(
			BuildSpec spec,
			String signature) {
		return spec.templates.containsKey(signature);
	}
	
	private final boolean isValidBlockMode(BuildSpec spec, BuildData bdata) {
		
		var facility = spec.facilities.get(bdata.oconn.facility);
		var specfnc = facility.get(bdata.oconn.function);
		
		switch (bdata.nextblockmode) {
		case 0:
			bdata.nextblockmode += specfnc.blockmode;
			return (bdata.nextblockmode <= 0);
		default:
			bdata.nextblockmode += specfnc.blockmode;
			return (bdata.nextblockmode == 0);
		}
	}
	
	private final CauldronProgram program(
			BuildSpec spec,
			String target,
			List<ProgramItem> items) {
		int i = 0;
		var context = new CauldronProgramContext();
		
		context.target = target;
		context.items = new ProgramItem[items.size()];
		for (var item : items)
		    context.items[i++] = item;
		
		context.name = new StringBuilder(context.target).
				append(":").
				append(spec.programid++).toString();
		
		context.input = spec.pinputs;
		
		return new CauldronProgram(context);
	}
	
	private final List<TestProgram> programsBuild(
			BuildSpec spec,
			ObjectConnection oconn,
			List<ProgramItem[]> pitems,
			List<Map<String, ProgramItemInput>> inputs) {
		var programs = new LinkedList<TestProgram>();
		
		if (inputs == null) {
			for (var items : pitems) {
				var bdata = copyShuffledItems(spec, items, null);
				bdata.oconn = oconn;
				programs.add(assemblyProgram(spec, bdata));
			}
			return programs;
		}
		
		for (var input : inputs)
		    if (isDataObjectOnly(input))
				for (var items : pitems) {
					var bdata = copyShuffledItems(spec, items, input);
					bdata.oconn = oconn;
					programs.add(assemblyProgram(spec, bdata));
				}
		
		return programs;
	}
	
	private final void programitembuild(BuildSpec spec, BuildData bdata)
			throws Exception {
		
		if (!isValidBlockMode(spec, bdata))
			return;
		
		var input = getCompatibleInput(spec, bdata);
		if (input != null) {
			programitemsetup(spec, bdata, input);
			return;
		}
		
	    if (getInput(spec, bdata.oconn).size() > 0)
	        return;
	    
	    bdata.expression = getCurrentExpression(bdata.oconn, new String[0]);
	    if (isEarlierBranch(spec, bdata.expression))
	        return;
	    
	    var item = new ProgramItem(
	            bdata.oconn.facility,
	            bdata.oconn.function,
	            (bdata.reference.pitem == null)?
	                    -1 : bdata.reference.pitem.index,
	            bdata.program.size());
	    
	    bdata.program.push(item);
	    item.source = bdata.source;
	    throw new AssemblySuccessException(bdata.expression);
	}
	
	private final void programitemcreate(ProgramItemSetupData pisetup)
	        throws Exception {
		int nrinputs = 0;
		Exception signal = null;
		
		var level = pisetup.bdata.program.size();
		if (level >= pisetup.spec.clevel)
			if (isInputStarving(pisetup))
				return;
		
		if (level > pisetup.spec.clevel) {
		    pisetup.spec.levellimit = true;
		    return;
		}
		
		var originallevel = pisetup.bdata.program.size();
		var bitem = clone(pisetup.reference);
		bitem.reference = pisetup.reference;
		
		bitem.pitem = new ProgramItem(
		        pisetup.oconn.facility,
		        pisetup.oconn.function,
		        (pisetup.reference.pitem == null)?
		        		-1 : pisetup.reference.pitem.index,
				pisetup.bdata.program.size());
		
		bitem.pitem.source = pisetup.bdata.source;
		pisetup.bdata.program.push(bitem.pitem);
		
		var tname = pisetup.spec.buildctx.target.getName();
		var protectconsumed = new HashSet<String>(pisetup.spec.consumed);
		var protectpinputs = new HashMap<String, String>(pisetup.spec.pinputs);
		
		for (var ikey : pisetup.input.keySet()) {
			var args = pisetup.input.get(ikey);
			if (args.function != null)
				continue;
			
			if (pisetup.spec.consumed.contains(args.object))
				continue;
			
			if (pisetup.bdata.nextblockmode != 0)
				continue;
			
			var okey = ParameterNameGet.execute(tname, ikey, bitem.pitem);
			bitem.pitem.input.put(okey, args);
			pisetup.spec.consumed.add(args.object);
			pisetup.spec.pinputs.put(okey, args.type);
			nrinputs++;
		}
		
		var blockmode = pisetup.bdata.nextblockmode;
		for (var ikey : pisetup.input.keySet()) {
			var args = pisetup.input.get(ikey);
			if (args.function == null)
			    continue;
			
			blockmode += args.blockmode;
			if (blockmode != 0)
				break;

			var okey = ParameterNameGet.execute(tname, ikey, bitem.pitem);
			bitem.pitem.input.put(okey, args);
			
			var test = pisetup.spec.test.titems.get(ikey);
			if (test != null) {
				args.program = new StringBuilder(tname).
						append("_").
						append(ikey).toString();
				
				if (args.blockmode == 0) {
					nrinputs++;
					continue;
				}
			}
			
			var oconn = pisetup.spec.ctxpool.cpool.instance(args.object);
			try {
				var bdata = pisetup.bdata.instance();
				bdata.reference = bitem;
				bdata.oconn = oconn;
				bdata.source = okey;
				programitembuild(pisetup.spec, bdata);
				break;
			} catch (AssemblySuccessException e) {
				signal = e;
				nrinputs++;
			}
		}
		
		if (nrinputs < pisetup.input.size()) {
			var currentlevel = pisetup.bdata.program.size();
			for (int i = originallevel; i < currentlevel; i++)
				pisetup.bdata.program.pop();
			
			pisetup.spec.pinputs = protectpinputs;
			pisetup.spec.consumed = protectconsumed;
			return;
		}
		
		protectpinputs.clear();
		protectconsumed.clear();
		
		throw (signal == null)?
		    new AssemblySuccessException(pisetup.bdata.expression) : signal;
	}
	
	private final void programitemnext(ProgramItemSetupData pisetup)
	        throws Exception {
		pisetup.bdata.expression = getExpression(pisetup,pisetup.bdata.program);
		
		if (!isEarlierBranch(pisetup.spec, pisetup.bdata.expression))
			programitemcreate(pisetup);
	}
	
	private final void programitemsetup(
			BuildSpec spec,
			BuildData bdata,
	        List<Map<String, ProgramItemInput>> inputs) throws Exception {
		
		var pisetup = new ProgramItemSetupData(bdata.reference, bdata.oconn);
		
		/*
		 * processaremos primeiramente parâmetros apenas com objetos de dados.
		 */
		pisetup.bdata = bdata;
		pisetup.spec = spec;
		for (var input : inputs) {
		    if (!(pisetup.dataonly = isDataObjectOnly(input)))
		        continue;
		    
		    pisetup.input = input;
		    programitemnext(pisetup);
		}
		
		/*
		 * processaremos agora qualquer parâmetro com retorno de função.
		 */
		for (var input : inputs) {
		    if ((pisetup.dataonly = isDataObjectOnly(input)))
		        continue;
		    
		    pisetup.input = input;
		    programitemnext(pisetup);
		}
	}
	
	private final void registerTemplate(
			String signature,
			BuildSpec spec,
			String facility,
			Map<String, String> input,
			String output,
			int blockmode) {
		
		var function = new StringBuilder("_template_").
				append(spec.templates.size()).toString();
		var tinput = finputconnect(spec, "template", function, input);
		
		spec.ctxpool.connect(
				"template",
				function,
				"template-output",
				"output",
				output);
		
		spec.templates.put(signature, new LinkedList<>());
		addFunction(
				spec,
				"template",
				function,
				tinput,
				output,
				true,
				false,
				blockmode);
	}
	
	private final void saveShuffledItems(
			TestProgram test,
			Map<String, ProgramItemInput> input) {
		
		var bdata = copyShuffledItems(
				test.spec,
				test.program.context.items,
				input);
		
		var name = getBranch(bdata.program).toString();
		test.spec.branches.add(name);
	}
	
	private final List<TestProgram> shuffle(TestProgram testprogram) {
		var specfnc = new SpecFunction(new HashMap<>(), null, false, false, 0);
		var ctxpool = cloneContextPool(testprogram.spec);
		for (var key : testprogram.spec.pinputs.keySet()) {
			var type = testprogram.spec.pinputs.get(key);
			ctxpool.connect("alpha", "test_one", "input", key, type);
			specfnc.input.put(key, type);
		}
		
		var oconn = new ObjectConnection();
		oconn.facility = "alpha";
		oconn.function = "test_one";
		
		var inputs = getCompatibleInput(
				testprogram.spec,
				testprogram.spec.test,
				ctxpool,
				oconn,
				null,
				specfnc,
				ALLOW_DUPS);
		
		if (inputs == null) {
			saveShuffledItems(testprogram, null);
			var pitems = shuffleProgramItems(
					testprogram.spec,
					testprogram.program.context.items,
					0);
			
			return programsBuild(testprogram.spec, oconn, pitems, null);
		}
		
		for (var input : inputs)
			saveShuffledItems(testprogram, input);
		
		var pitems = shuffleProgramItems(
				testprogram.spec,
				testprogram.program.context.items,
				0);
		
		return programsBuild(testprogram.spec, oconn, pitems, inputs);
	}
	
	private final List<ProgramItem[]> shuffleProgramItems(
	        BuildSpec spec,
	        ProgramItem[] items,
	        int itemnr) {
		var programs = new LinkedList<ProgramItem[]>();
		shuffleProgramItems(spec, programs, items, itemnr, 0);
		return programs;
	}
	
	private final void shuffleProgramItems(
			BuildSpec spec,
			List<ProgramItem[]> programs,
			ProgramItem[] items,
			int itemnr,
			int blocks) {
		int ditemnr;
		var sitem = items[itemnr];
		var sfnccfg = spec.facilities.get(sitem.facility).get(sitem.function);
		
		blocks += sfnccfg.blockmode;
		if (!sfnccfg.template) {
			if (items.length == 1) {
				if (blocks == 0)
					programs.add(items);
			} else {
			    shuffleProgramItems(spec, programs, items, itemnr + 1, blocks);
			}
			return;
		}
		
		if (sitem.reference > -1)
			if (!items[sitem.reference].input.containsKey(sitem.source))
				return;
		
		var tinputs = new HashSet<String>();
		var signature = getFunctionSignature(
				sfnccfg.input,
				sfnccfg.output,
				sfnccfg.blockmode);
		
		var tname = spec.buildctx.target.getName();
		
		for (var function : spec.templates.get(signature)) {
			var ditems = new ProgramItem[items.length];
			
			ditems[itemnr] = new ProgramItem(
			        function[0],
			        function[1],
			        sitem.reference,
			        sitem.index);
			
			ditems[itemnr].source = sitem.source;
			var finputs = spec.facilities.get(function[0]).get(function[1]).input;
			for (var key : finputs.keySet()) {
				if (isFunction(sitem, key)) {
				    ditems[itemnr].input.put(key, sitem.input.get(key));
				    continue;
				}
				
				var sitemkey = ParameterNameGet.execute(
						tname,
						key,
						ditems[itemnr]);
				
				var args = sitem.input.get(sitemkey);
				if (args.ignore) {
					ditems[itemnr].input.put(sitemkey, args);
					continue;
				}
				
				if (args.program == null) {
					var dobj = spec.test.datactx.get(args.object);
					if (!dobj.getType().isCompatible(finputs.get(key)))
					    return;
					
					if (tinputs.contains(args.object))
					    continue;
					
					tinputs.add(args.object);
				}
				
				ditems[itemnr].input.put(sitemkey, args);
			}
			
			tinputs.clear();
			if (ditems[itemnr].input.size() != finputs.size())
				continue;
			
			for (int i = 0; i < items.length; i++)
				if (i != itemnr)
				    ditems[i] = items[i];
			
			var item = ditems[sitem.reference];
			ditems[sitem.reference] = clone(
					spec,
					null,
					null,
					item,
					new HashMap<>());
			
			if ((ditemnr = itemnr + 1) < items.length) {
				shuffleProgramItems(spec, programs, ditems, ditemnr, blocks);
				continue;
			}
			
			if (blocks == 0)
				programs.add(ditems);
		}
	}
	
	private final BuildSpec specsetup(BuildContext buildctx) {
		var spec = new BuildSpec();
		spec.buildctx = buildctx;
		spec.clevel = spec.buildctx.pinlevel;
		spec.reference = new BuildItem();
		
		if (spec.buildctx.target.tests.size() == 0)
			spec.test = spec.buildctx.target.test();
		else
			spec.test = spec.buildctx.target.tests.get(0);
		
		buildinputconns(spec);
		buildfunctionsconns(spec);
		
		return spec;
	}
	
	private final boolean testContinue(TestProgram test) {
		if (!isApproved(test.spec.validated, test))
		    return true;
		
		test.spec.validated.put(test.program.context.target, test.program);
		if (test.spec.buildctx.resultlimit == 0)
		    return true;
		
		test.spec.resultc++;
		return (test.spec.resultc != test.spec.buildctx.resultlimit);
	}
}

class TerminateException extends Exception {
	private static final long serialVersionUID = -7672817764073950224L;
}

class TestProgram {
	public CauldronProgram program;
	public BuildSpec spec;
	public ObjectConnection oconn;
	
	public TestProgram(CauldronProgram program, BuildSpec spec) {
		this.program = program;
		this.spec = spec;
	}
}
