package org.quanticsoftware.automata.cauldron;

public class ObjectConnection {
	public String name, facility, function, parent, direction;
	public String parameter;
	
	@Override
	public final String toString() {
		return name;
	}
}

