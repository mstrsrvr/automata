package org.quanticsoftware.automata.cauldron;

public class ParameterNameGet {
	
	public static final String execute(
			String target,
			String name,
			ProgramItem item) {
		return new StringBuilder(target).append(".").
				append(name).append(":").
				append(item.index).toString();
	}
	
}
