package org.quanticsoftware.automata.cauldron;

import java.util.LinkedHashMap;
import java.util.Map;

public class ProgramItem {
	public CauldronProgram program;
	public String facility, function, source, output;
	public Map<String, ProgramItemInput> input;
	public int index, reference;
	public boolean dontclone;
	
	public ProgramItem(String facility, String function, int refid, int id) {
		this.facility = facility;
		this.function = function;
		index = id;
		reference = refid;
		input = new LinkedHashMap<>();
		output = GetParameterName.run(facility, function, "output");
	}
	
	@Override
	public final String toString() {
		var sb = new StringBuilder(facility).append(".").
				append(function).append("(");
		String value = null;
		for (var key : input.keySet()) {
			if (value != null)
				sb.append(",");
			var args = input.get(key);
			sb.append(value = args.object);
		}
		return sb.append(")").toString();
	}
}
