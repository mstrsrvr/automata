package org.quanticsoftware.automata.cauldron;

public class AssemblySuccessException extends Exception {
    private static final long serialVersionUID = -7645478812438709841L;
    public String branch;
    
    public AssemblySuccessException(String branch) {
        this.branch = branch;
    }
}

