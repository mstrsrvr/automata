package org.quanticsoftware.automata.cauldron;

public class BuildItem {
	public String outputname, source;
	public BuildItem reference;
	public ProgramItem pitem;
	public ObjectConnection oconn;
	
	@Override
	public final String toString() {
		if (oconn == null)
		    return super.toString();
		if (oconn.facility == null)
		    return oconn.toString();
		return new StringBuilder(oconn.parameter).append(" = ").
				append(pitem.toString()).toString();
	}
}
