package org.quanticsoftware.automata.cauldron;

import java.util.Map;

public class SpecFunction {
	public String output;
	public Map<String, String> input;
	public boolean template, passthrough;
	public int blockmode;
	
	public SpecFunction(
			Map<String, String> input,
			String output,
			boolean template,
			boolean passthrough,
			int blockmode) {
		this.input = input;
		this.output = output;
		this.template = template;
		this.passthrough = passthrough;
		this.blockmode = blockmode;
	}
}