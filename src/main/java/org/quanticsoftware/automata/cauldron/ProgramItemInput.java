package org.quanticsoftware.automata.cauldron;

public class ProgramItemInput {
	public String input, object, facility, function, type, program;
	public boolean ignore;
	public int blockmode;
	
	private final void token(StringBuilder sb, String name, String value) {
		sb.append(name).append((input == null)? "null" : value);
	}
	
	@Override
	public final String toString() {
		var sb = new StringBuilder();
		token(sb, "input: ", input);
		token(sb, ", object: ", object);
		token(sb, ", facility: ", facility);
		token(sb, ", function: ", function);
		token(sb, ", type: ", type);
		token(sb, ", program: ", program);
		token(sb, ", ignore: ", Boolean.toString(ignore));
		return sb.toString();
	}
}
