package org.quanticsoftware.automata.cauldron;

import java.util.Stack;

public class BuildData {
	public int levellimit, nextblockmode;
	public BuildItem reference;
	public ObjectConnection oconn;
	public String source, expression;
	public Stack<ProgramItem> program;
	
	public BuildData() {
		program = new Stack<>();
	}
	
	public final BuildData instance() {
		var bdata = new BuildData();
		bdata.program = program;
		bdata.nextblockmode = nextblockmode;
		return bdata;
	}
}
