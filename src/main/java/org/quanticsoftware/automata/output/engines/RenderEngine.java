package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public interface RenderEngine {
	public static final boolean NO_ID = true;

	public abstract boolean hasNoPrintedId();
	
	public abstract boolean isRenderingDelayed();
	
	public abstract XMLElement run(RenderContext renderctx);
	
}

