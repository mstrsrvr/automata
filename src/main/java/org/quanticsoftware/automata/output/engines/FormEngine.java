package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class FormEngine extends AbstractRenderEngine {

	public FormEngine() {
		super("form");
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		renderctx.currentform = renderctx.element.name;
		renderctx.currentaction = renderctx.element.action;
		output.add("method", "post");
	}
	
}
