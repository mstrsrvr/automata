package org.quanticsoftware.automata.output;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.application.ContextEntry;
import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.TypeContext;

public class RenderContext {
	private ContextEntry ctxentry;
	public PageContext page;
	public PageElement element;
	public String currentform, currentaction, updtarea, action, msgtext;
	public String indexpage, msgstatus;
	public Object[] msgargs;
	public TypeContext typectx;
	public boolean laterendering;
	public Set<Object[]> delayed;
	public XMLElement output;
	public DataContext datactx;
	public Map<String, DataObject> objects;
	public Locale locale;
	public Exception ex;
	
	public RenderContext(ContextEntry ctxentry) {
		delayed = new HashSet<>();
		datactx = new DataContext();
		this.ctxentry = ctxentry;
	}
	
	public final void add(DataObject object) {
		ctxentry.add(object);
	}
	
	public final void input(String element) {
		ctxentry.inputs.add(element);
	}
	
	public static final RenderContext instance(ContextEntry ctxentry) {
		var renderctx = new RenderContext(ctxentry);
		renderctx.updtarea = ctxentry.updtarea;
		renderctx.action = ctxentry.action;
		renderctx.objects = ctxentry.objects;
		renderctx.locale = ctxentry.locale;
		renderctx.msgtext = ctxentry.msgtext;
		renderctx.msgstatus = ctxentry.msgstatus;
		renderctx.msgargs = ctxentry.msgargs;
		renderctx.indexpage = ctxentry.indexpage;
		renderctx.ex = ctxentry.ex;
		ctxentry.inputs.clear();
		return renderctx;
	}
}
