package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.cauldron.CauldronProgram;

public class InputData {
	public CauldronProgram program;
	public InputDataObjects objects;
}
