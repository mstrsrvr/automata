package org.quanticsoftware.automata.runtime;

public interface FunctionRule {
	
	public abstract void execute(Session session);
	
}
