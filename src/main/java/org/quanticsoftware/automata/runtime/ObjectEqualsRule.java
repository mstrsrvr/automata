package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.GetLeastType;

public class ObjectEqualsRule implements TargetRule {
	
	@Override
	public boolean isValid(Test test, DataObject document) {
		var source = GetLeastType.get(test.getType());
		return GetLeastType.get(document.getType()).equals(source);
	}
	
}
