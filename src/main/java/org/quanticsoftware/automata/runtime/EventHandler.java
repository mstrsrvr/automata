package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.application.ContextEntry;

public interface EventHandler {
	
	public abstract void run(ContextEntry ctxentry, Object parameter);
	
}
