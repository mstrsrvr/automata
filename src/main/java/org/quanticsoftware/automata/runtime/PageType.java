package org.quanticsoftware.automata.runtime;

public class PageType {
	private String page, element;
	private boolean auto;
	public String engine;
	
	public PageType(String page, String element, boolean auto) {
		this.page = page;
		this.element = element;
		this.auto = auto;
	}
	
	public final String element() {
		return element;
	}
	
	public final boolean isAutomatic() {
		return auto;
	}
	
	public final String page() {
		return page;
	}
}
