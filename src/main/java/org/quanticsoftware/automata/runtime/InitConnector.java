package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.runtime.connector.Connector;

public interface InitConnector {
	
	public abstract Connector instance();
	
}
