package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.output.PageContext;

public interface PageSpec {
	
	public abstract void execute(PageContext pagectx);
}

