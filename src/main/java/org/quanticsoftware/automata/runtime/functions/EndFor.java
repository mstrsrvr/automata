package org.quanticsoftware.automata.runtime.functions;

import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.GetLeastType;
import org.quanticsoftware.automata.runtime.Facility;

public class EndFor {

	public static final void execute(Facility facility, DataType type) {
		if (!type.isCollection())
			return;
		
		var otype = GetLeastType.get(type);
		var oname = otype.getPath();
		var fname = String.format("%s_end_for", oname);
		if (facility.functions().contains(fname))
			return;
		
		var iname = GetLeastType.get(otype.getElement()).getPath();
		var function = facility.function(fname);
		function.input(iname, "item");
		function.output(oname);
		function.endblock();
		function.rule((s)->{
			var codeblock = s.codeblock();
			
			if (codeblock.items == null)
				codeblock.items = s.output();
			
			var item = s.input.get("item");
			var oitem = codeblock.items.instance();
			for (var key : item.getItems())
				oitem.set(key, item.getItem(key));
			
			codeblock.leave = (codeblock.keys.size() == 0);
		});
	}
}
