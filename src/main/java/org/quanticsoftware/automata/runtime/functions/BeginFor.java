package org.quanticsoftware.automata.runtime.functions;

import java.util.Stack;

import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.GetLeastType;
import org.quanticsoftware.automata.runtime.Facility;

public class BeginFor {
	
	public static final void execute(Facility facility, DataType type) {
		if (!type.isCollection())
			return;

		var itype = GetLeastType.get(type);
		var iname = itype.getPath();
		var fname = String.format("%s_begin_for", iname);
		if (facility.functions().contains(fname))
			return;
		
		var oname = GetLeastType.get(itype.getElement()).getPath();
		var function = facility.function(fname);
		function.input(iname, "items");
		function.output(oname);
		function.beginblock();
		function.rule((s)->{
			var items = s.input.get("items");
			var codeblock = s.codeblock();
			
			if (codeblock.keys == null) {
				codeblock.keys = new Stack<String>();
				var keys = items.getItems().toArray();
				for (int i = keys.length - 1; i >= 0; i--)
					codeblock.keys.push((String)keys[i]);
			}
			
			var item = items.getItem(codeblock.keys.pop());
			var output = s.output();
			for (var key : item.getItems())
				output.set(key, item.getItem(key));
		});
	}
}
