package org.quanticsoftware.automata.runtime.functions;

import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.GetLeastType;
import org.quanticsoftware.automata.runtime.Facility;

public class Members2ParentMov {
	
	public static final void execute(Facility facility, DataType type) {
		
		if (type.getItems().size() == 0)
			return;
		
		var ptype = GetLeastType.get(type).getPath();
		var fname = String.format("members_to_%s_mov", ptype);
		if (facility.functions().contains(fname))
			return;

		var function = facility.function(fname);
		for (String key : type.getItems()) {
			var mtype = GetLeastType.get(type.get(key));
			function.input(mtype.getPath(), key);
		}
		
		function.output(ptype);
		function.rule((s)->{
			var output = s.output();
			for (var key : output.getItems()) {
				var item = s.input.get(key);
				if (item != null)
					output.set(key, item);
			}
		});
	}

}
