package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.application.DataObject;

public class CauldronFacility implements Facility {
	private Map<String, FunctionContext> functions;
	private String name;
	
	public CauldronFacility(String name) {
		functions = new HashMap<>();
		this.name = name;
	}
	
	@Override
	public final Function function(String name) {
		var functionctx = functions.get(name);
		if (functionctx == null)
			functions.put(name,
					functionctx = new FunctionContext(this.name, name));
		return functionctx.function;
	}

	@Override
	public final Set<String> functions() {
		return functions.keySet();
	}
	
	@Override
	public final String getAbsoluteName(String function) {
		return functions.get(function).absname;
	}
	
	@Override
	public final DataObject run(String function, SharedSession shsession) {
		return functions.get(function).function.run(shsession);
	}
}

class FunctionContext {
	public Function function;
	public String absname;
	
	public FunctionContext(String facility, String name) {
		function = new CauldronFunction(name);
		absname = new StringBuilder(facility).
				append(".").append(name).toString();
	}
}
