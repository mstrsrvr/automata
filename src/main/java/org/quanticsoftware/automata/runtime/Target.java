package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.application.DataType;

public class Target {
	private String name;
	private DataType type;
	public List<Test> tests;
	public Set<String> required, testonly;
	public Map<String, DataType> input;
	
	public Target(DataType type, String name) {
		this.type = type;
		this.name = name;
		tests = new LinkedList<>();
		required = new HashSet<>();
		testonly = new HashSet<>();
		input = new HashMap<>();
	}
	
	public final String getName() {
		return name;
	}
	
	public final DataType getType() {
		return type;
	}
	
	public final void input(DataType type, String name) {
		input.put(name, type);
	}
	
	public final Test test() {
		Test test = new Test(type, this);
		tests.add(test);
		return test;
	}
}
