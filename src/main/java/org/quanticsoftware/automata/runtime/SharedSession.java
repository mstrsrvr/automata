package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.runtime.connector.DBSession;

public class SharedSession {
	public Runtime rt;
	public String outputtype, blockid;
	public DataContext datactx;
	public Map<String, DataObject> input;
	public DBSession dbsession;
	public Map<String, CodeBlock> codeblocks;
	public Stack<String> blockstack;
	
	public SharedSession() {
		codeblocks = new HashMap<>();
		blockstack = new Stack<>();
	}
	
	public final CodeBlock codeblock(String blockid) {
		var codeblock = codeblocks.get(blockid);
		if (codeblock != null)
			return codeblock;
		codeblocks.put(blockid, codeblock = new CodeBlock());
		return codeblock;
	}
}
