package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;

public class Test {
	private DataType type;
	private Target target;
	public DataContext datactx;
	public DatabaseRule database;
	public TargetRule rule;
	public Map<String, Test> titems;
	
	public Test(DataType type, Target target) {
		this.type = type;
		this.target = target;
		datactx = new DataContext();
		titems = new HashMap<>();
	}
	
	public final DataType getType() {
		return type;
	}
	
	public final DataObject input(String name) {
		return datactx.instance(target.input.get(name), null, name);
	}
	
	public final void input(String name, DataObject object) {
		datactx.copy(name, object);
	}
	
	public final Test instance(String name) {
		Test test = titems.get(name);
		if (test != null)
			return test;
		
		var type = this.type.getElement();
		if (type != null) {
			titems.put("item", test = new Test(type, target));
			return test.instance(name);
		}
		
		type = this.type;
		var tokens = name.split("\\.");
		for (var token : tokens) {
			if (test == null) {
				test = titems.get(token);
				if (test != null)
					continue;
			}
			
			if (test != null)
				test = test.instance(token);
			else
				titems.put(token, test = new Test(type.get(token), target));
		}
		
		return test;
	}
}
