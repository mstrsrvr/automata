package org.quanticsoftware.automata.runtime;

public class Page {
	private String name;
	public PageSpec spec;
	public PageConfig config;
	
	public Page(String name) {
		this.name = name;
	}
	
	public final String getName() {
		return name;
	}
}
