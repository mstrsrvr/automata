package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.cauldron.ProgramState;

public class SuspendProgramException extends Exception {
	private static final long serialVersionUID = -7054681488962421634L;
	public ProgramState state;
	
	public SuspendProgramException(ProgramState state) {
		this.state = state;
	}
}
