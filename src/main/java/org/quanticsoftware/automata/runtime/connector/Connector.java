package org.quanticsoftware.automata.runtime.connector;

import java.util.Set;

import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataType;

public interface Connector {
    
    public abstract void create(DataType type);
    
    public abstract Set<String> getDocuments();
    
    public abstract DBSession instance(DataContext datactx);
}
