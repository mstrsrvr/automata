package org.quanticsoftware.automata.runtime.connector;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.GeneralException;

public class DummySession implements DBSession {
	private boolean transactionstarted;
	private Map<String, Map<Object, DataObject>> database, session;
	
	public DummySession(Map<String, Map<Object, DataObject>> database) {
		this.database = database;
		session = new HashMap<>();
		for (var key : database.keySet())
			session.put(key, new HashMap<>());
	}
	
	@Override
	public final void close() { };
	
	@Override
	public final void commit() {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
		for (var tkey : session.keySet()) {
			var records = session.get(tkey);
			for (var rkey : records.keySet())
				database.get(tkey).put(rkey, records.get(rkey));
		}
		transactionstarted = false;
	}
	
	@Override
	public final DataObject get(DataType type, Object key) {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
		return database.get(type.getName()).get(key);
	}
	
	@Override
	public final void insert(DataObject data) {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
	}
	
	@Override
	public final boolean isTransactionStarted() {
		return transactionstarted;
	}
	
	@Override
	public final void rollback() {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
		for (var key : session.keySet())
			session.get(key).clear();
		transactionstarted = false;
	}
	
	@Override
	public final void save(DataObject data) {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
		update(data);
	}
	
	@Override
	public final void start() {
		if (transactionstarted)
			throw new GeneralException("session already started.");
		transactionstarted = true;
	}
	
	@Override
	public final void update(Query query) {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
	}
	
	@Override
	public final void update(DataObject data) {
		if (!transactionstarted)
			throw new GeneralException("transaction not started.");
		var type = data.getType();
		var table = session.get(type.getName());
		table.put(data.get(type.getKey()), data);
	}
    
}