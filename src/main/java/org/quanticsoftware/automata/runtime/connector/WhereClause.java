package org.quanticsoftware.automata.runtime.connector;

import java.util.HashMap;
import java.util.Map;

public class WhereClause {
    public static final byte EQ = 0;
    public static final byte NE = 1;
    public static final byte LT = 2;
    public static final byte LE = 3;
    public static final byte GT = 4;
    public static final byte GE = 5;
    public static final byte IN = 6;
    public static final byte BE = 7;
    public static final byte EE = 8;
    public static final byte EQ_ENTRY = 9;
    public static final byte NE_ENTRY = 10;
    public static final byte LT_ENTRY = 11;
    public static final byte LE_ENTRY = 12;
    public static final byte GT_ENTRY = 13;
    public static final byte GE_ENTRY = 14;
    public static final byte IN_ENTRY = 15;
    public static final byte BE_ENTRY = 16;
    public static final byte EE_ENTRY = 17;
    public static final byte CP = 18;
    public static final byte RG = 19;
    public static final byte BT = 20;
    private String field, operator;
    private byte condition;
    private Object value;
    private Map<Byte, String> operators;
    
    public WhereClause(String field, byte condition, Object value,
            String operator) {
        this.field = field;
        this.condition = condition;
        this.value = value;
        this.operator = operator;
        
        operators = new HashMap<>();
        operators.put(EQ, " = ");
        operators.put(NE, " <> ");
        operators.put(LT, " < ");
        operators.put(LE, " <= ");
        operators.put(GT, " > ");
        operators.put(GE, " => ");
        operators.put(IN, " in ");
        operators.put(BE, " ( ");
        operators.put(EE, " ) ");
        operators.put(EQ_ENTRY, " =[] ");
        operators.put(NE_ENTRY, " <>[] ");
        operators.put(LT_ENTRY, " <[] ");
        operators.put(LE_ENTRY, " <=[] ");
        operators.put(GT_ENTRY, " >[] ");
        operators.put(GE_ENTRY, " =>[] ");
        operators.put(IN_ENTRY, " in[] ");
        operators.put(BE_ENTRY, " ([] ");
        operators.put(EE_ENTRY, " []) ");
        operators.put(CP, " like ");
        operators.put(RG, " in ");
        operators.put(BT, " => <= ");
    }

    public final byte getCondition() {
        return condition;
    }
    
    public final String getField() {
        return field;
    }

    public final String getOperator() {
        return operator;
    }

    public final Object getValue() {
        return value;
    }
    
    @Override
    public final String toString() {
        var op = operators.get(condition);
        return new StringBuilder((operator == null)? "" : operator).
                append(" ").
                append((field == null)? "" : field).
                append(op).
                append((value == null)? "null" : value.toString()).toString();
    }
}
