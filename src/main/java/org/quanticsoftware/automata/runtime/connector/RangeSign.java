package org.quanticsoftware.automata.runtime.connector;

public enum RangeSign {
    INCLUDE, EXCLUDE
}
