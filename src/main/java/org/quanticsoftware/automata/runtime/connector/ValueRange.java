package org.quanticsoftware.automata.runtime.connector;

import java.util.ArrayList;
import java.util.List;

public class ValueRange {
    private List<ValueRangeItem> itens;
    
    public ValueRange() {
        itens = new ArrayList<>();
    }
    
    public final void add(ValueRangeItem item) {
        itens.add(item);
    }
    
    public final ValueRangeItem get(int index) {
        return itens.get(index);
    }
    
    public final List<ValueRangeItem> getItens() {
        return itens;
    }
    
    public final int length() {
        return itens.size();
    }
    
    @Override
    public final String toString() {
        return itens.toString();
    }
}
