package org.quanticsoftware.automata.runtime;

import java.util.Map;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.ErrorMessageException;
import org.quanticsoftware.automata.application.GeneralException;
import org.quanticsoftware.automata.runtime.connector.DBSession;

public class Session {
	private SharedSession shsession;
	private DataObject output;
	public Map<String, DataObject> input;
	public DBSession dbsession;
	
	public Session(SharedSession shsession) {
		this.shsession = shsession;
		input = shsession.input;
		dbsession = shsession.dbsession;
	}
	
	public final DataObject call(String target) {
		var program = shsession.rt.context().programs.get(target);
		if (program == null)
			throw new GeneralException(
					"program not found for target '%s'.", target);
		var idata = new InputData();
		idata.program = program;
		idata.objects = (k)->shsession.datactx.get(k);
		var input = InputGet.execute(idata);
		try {
			return shsession.rt.run(dbsession, program, input);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public final CodeBlock codeblock() {
		return shsession.codeblocks.get(shsession.blockid);
	}
	
	public final void error(String text, Object... args) {
		throw new ErrorMessageException(text, args);
	}
	
	public final double getd(String input) {
		return this.input.get(input).getd();
	}
	
	public final int geti(String input) {
		return this.input.get(input).geti();
	}
	
	public final String getst(String input) {
		return this.input.get(input).getst();
	}
	
	public final DataObject output() {
		if (output != null)
			return output;
		var type = shsession.rt.context().typectx().get(shsession.outputtype);
		return output = shsession.datactx.instance(type);
	}
	
	public final void set(DataObject output) {
		if (output == null)
			throw new GeneralException("output parameter can't be null");
		this.output = output;
	}
}
