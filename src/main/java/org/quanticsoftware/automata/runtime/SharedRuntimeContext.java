package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

public class SharedRuntimeContext {
	public Map<String, Facility> facilities;
	public Map<String, Map<String, PageType>> pagetypes;
	public Map<String, Page> pages;
	
	public SharedRuntimeContext() {
		facilities = new HashMap<>();
		pages = new HashMap<>();
		pagetypes = new HashMap<>();
	}
	
	public final PageType pagetype(
			String page, String datatype, String element, boolean auto) {
		var pagetypes = this.pagetypes.get(datatype);
		if (pagetypes == null)
			this.pagetypes.put(datatype, pagetypes = new HashMap<>());
		var pagetype = pagetypes.get(element);
		if (pagetype == null)
			pagetypes.put(page, pagetype = new PageType(page, element, auto));
		return pagetype;
	}
}
