package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.output.PageContext;

public interface PageConfig {
	
	public abstract void execute(PageContext pagectx);
	
}
