package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.ContextEntry;
import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.GeneralException;
import org.quanticsoftware.automata.cauldron.BuildContext;
import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.cauldron.CauldronProgram;
import org.quanticsoftware.automata.cauldron.ParameterNameGet;
import org.quanticsoftware.automata.cauldron.ProgramItem;
import org.quanticsoftware.automata.cauldron.ProgramState;
import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.runtime.connector.Connector;
import org.quanticsoftware.automata.runtime.connector.DBSession;
import org.quanticsoftware.automata.runtime.connector.DummyConnector;
import org.quanticsoftware.automata.runtime.functions.BeginFor;
import org.quanticsoftware.automata.runtime.functions.EndFor;
import org.quanticsoftware.automata.runtime.functions.Members2ParentMov;
import org.quanticsoftware.automata.runtime.functions.ObjectGet;
import org.quanticsoftware.automata.runtime.functions.Passthrough;

public class Runtime {
	private RuntimeContext rtctx;
	private boolean nopagecheck, dontbuildprograms, usedummyconn;
	private Connector connector;
	public SharedRuntimeContext shrtctx;
	
	public Runtime() {
		shrtctx = new SharedRuntimeContext();
		rtctx = new RuntimeContext(shrtctx);
	}
	
	public final Map<String, CauldronProgram> build(
			Cauldron cauldron,
			Target target,
			RuntimeInit rtinit) {
		
		var buildctx = new BuildContext();
		buildctx.target = target;
		buildctx.rtinit = rtinit;
		buildctx.rtctx = rtctx;
		buildctx.resultlimit = 1;
		
		var programs = cauldron.build(buildctx);
		if (programs == null)
			fail("target '%s' build failed.", target.getName());
		
		return programs;
	}
	
	public final PageContext build(Page page) {
		var pagectx = new PageContext();
		page.spec.execute(pagectx);
		if (page.config != null)
			page.config.execute(pagectx);
		return pagectx;
	}
	
	private final void buildPrograms(RuntimeInit appinit) {
		var cauldron = new Cauldron();
		
		for (var key : rtctx.targets.keySet()) {
			if (rtctx.programs.containsKey(key))
				continue;
			
			var target = rtctx.targets.get(key);
			if (target.getType() == null)
				fail("target '%s' has undefined expected return.", key);

			rtctx.programs.putAll(build(cauldron, target, appinit));
		}
	}
	
	private final DataObject cataloginit() {
		var typectx = rtctx.typectx();
		
		var finput_t = typectx.define("finput_t");
		finput_t.addst("name");
		finput_t.addst("type");
		var fiparams_t = typectx.array(finput_t, "fiparams_t");
		
		var function_t = typectx.define("function_t");
		function_t.addst("name");
		function_t.add(fiparams_t, "input");
		function_t.addst("output");
		function_t.addbl("passthrough");
		function_t.addi("block_mode");
		var functions_t = typectx.map(function_t, "functions_t", "name");
		
		var facility_t = typectx.define("facility_t");
		facility_t.addst("name");
		facility_t.add(functions_t, "functions");
		var facilities_t = typectx.map(facility_t, "facilities_t", "name");
		
		var program_t = typectx.define("program_t");
		program_t.addst("name");
		program_t.addst("code");
		var programs_t = typectx.array(program_t, "programs_t");

		var context_t = typectx.define("context");
		context_t.add(facilities_t, "facilities");
		context_t.add(programs_t, "programs");
		context_t.addbl("active");
		
		var catalog = rtctx.datactx.instance(context_t, "context");
		catalog.set("active", true);
		return catalog;
	}
	
	public final RuntimeContext context() {
		return rtctx;
	}
	
	private final void copy(
			Map<String, DataObject> objects,
			String name,
			DataObject object) {
		objects.put(name, object);
		for (var key : object.getItems())
			objects.put(name.concat(".").concat(key), object.getItem(key));
	}

	private final void dbinit() {
		connector = ((rtctx.initconn == null) || usedummyconn)?
				new DummyConnector() : rtctx.initconn.instance();
		var documents = connector.getDocuments();
		
		var typectx = rtctx.typectx();
		for (var key : typectx.types()) {
			var type = typectx.get(key);
			if ((type.getKey() != null) && !documents.contains(key))
				connector.create(type);
		}
	}
	
	public final void dontBuildPrograms() {
		dontbuildprograms = true;
	}
	
	private final void fail(String text, Object... args) {
		throw new GeneralException(text, args);
	}
	
	private final void functionscatalog(DataObject catalog) {
		var cfacilities = catalog.getItem("facilities");
		for (var fackey : shrtctx.facilities.keySet()) {
			var cfacility = cfacilities.instance(fackey);
			var cfunctions = cfacility.getItem("functions");
			
			var facility = shrtctx.facilities.get(fackey);
			for (var fnckey : facility.functions()) {
				var cfunction = cfunctions.instance(fnckey);
				facility.function(fnckey).config(cfunction);
				
				var message = validateFunctionConfig(fnckey, cfunction);
				if (message != null)
					fail(message);
			}
		}
	}
	
	private final void functionsinit() {
		var facility = rtctx.facility("runtime");
		var typectx = rtctx.typectx();
		
		for (var tkey : typectx.types()) {
			var type = typectx.get(tkey);
			Members2ParentMov.execute(facility, type);
			ObjectGet.execute(facility, type);
			Passthrough.execute(facility, type);
			BeginFor.execute(facility, type);
			EndFor.execute(facility, type);
		}
	}
	
	private final String getBlockId(ProgramItem item) {
		return new StringBuilder(item.facility).append(".").
			append(item.function).append(":").
			append(item.index).toString();
	}
	
	private final DataObject getFunctionConfig(
			DataContext datactx, String facility, String function) {
		return datactx.get("context.facilities").getItem(facility).
				getItem("functions").getItem(function);
	}
	
	private final FunctionInput getFunctionInput(
			RunParameters runp,
			String target,
			ProgramItem item) throws Exception {
		
		var finput = new FunctionInput();
		var inputcfg = getFunctionConfig(
				runp.rtdatactx,
				item.facility,
				item.function).getItem("input");
		
		for (var ikey : inputcfg.getItems()) {
			var inputitem = inputcfg.getItem(ikey);
			var finame = inputitem.getst("name");
			var iname = ParameterNameGet.execute(target, finame, item);
			
			var args = item.input.get(iname);
			if (args.ignore)
				continue;
			
			var value = runp.input.get(iname);
			
			/*
			 * verificamos se o dado foi deixado no mapa por alguma das
			 * funções anteriores ou se vêm de um parâmetro de entrada.
			 */
			if (value == null) {
				var reference = runp.references.get(item.index);
				if (reference != null) {
					var retdoc = reference.get(finame);
					if (retdoc != null)
						value = runp.input.get(retdoc);
				}
			}
			
			if ((args.object != null) && (value == null))
				value = runp.input.get(args.object);
			
			if (args.program != null) {
				var crunp = new RunParameters();
				crunp.references = runp.references;
				crunp.rtdatactx = runp.rtdatactx;
				crunp.shsession.dbsession = runp.shsession.dbsession;
				crunp.programs = runp.programs;
				
				var program = (runp.programs == null)?
						rtctx.programs.get(args.program) :
							runp.programs.get(args.program);
				
				if (value == null) {
					crunp.input = runp.input;
					crunp.datactx = runp.datactx;
				} else {
					crunp.datactx = new DataContext();
					crunp.input = new HashMap<>();
					
					var type = value.getType().getPath();
					for (var test : program.context.buildctx.target.tests)
						for (var key : test.datactx.references(type))
							copy(crunp.input, key, value);
				}
				
				value = run(crunp, program);
			}
			
			if (value == null) {
				var itype = inputitem.getst("type");
				finput.state = getState(item, target, iname, itype);
				break;
			}
			
			finput.input.put(finame, value);
		}
		
		return finput;
	}
	
	private final ProgramState getState(
			ProgramItem item,
			String target,
			String input,
			String type) {
		
		var state = new ProgramState();
		state.reason = "missing_input";
		state.item = item;
		state.ftype = type;
		state.finput = input;
		state.target = target;
		
		return state;
	}
	
	public final String init(RuntimeInit appinit) {
		var catalog = cataloginit();
		var action = appinit.init(rtctx);
		
		if (!nopagecheck)
			pagesinit();
		
		dbinit();
		functionsinit();
		functionscatalog(catalog);
		
		if (!dontbuildprograms) {
			buildPrograms(appinit);
			programscatalog(catalog);
		}
		
		return action;
	}
	
	public final void noPageCheck() {
		nopagecheck = true;
	}
	
	private final void pagesinit() {
		var typectx = rtctx.typectx();
		
		for (var pkey : shrtctx.pages.keySet()) {
			var page = shrtctx.pages.get(pkey);
			var pagectx = build(page);
			
			for (var ekey : pagectx.elements()) {
				var element = pagectx.get(ekey);
				
				if ((element.engine != null) &&
						!rtctx.engines.containsKey(element.engine))
					fail("invalid engine '%s' at element '%s' at page '%s'.",
							element.engine, ekey, pkey);
				
				if (element.datatype == null)
					continue;
				
				if (typectx.get(element.datatype) == null)
					fail("invalid type '%s' at element '%s' at page '%s'.",
							element.datatype, ekey, pkey);
				
				if (element.notypeindexing)
					continue;
				
				shrtctx.pagetype(pkey, element.datatype, ekey, false);
			}
		}
	}
	
	private final void programscatalog(DataObject catalog) {
		var programs_t = rtctx.typectx().get("programs_t");
		var cprograms = rtctx.datactx.instance(programs_t, "programs");
		
		for (var key : rtctx.programs.keySet()) {
			var program = rtctx.programs.get(key);
			var cprogram = cprograms.instance();
			cprogram.set("name", key);
			cprogram.set("code", program.toString());
		}
		
		catalog.set("programs", cprograms);
	}
	
	private final DataObject run(
			RunParameters runp,
			CauldronProgram program,
			ProgramItem item) throws Exception {

		CodeBlock codeblock;
		
		var finput = getFunctionInput(runp, program.context.target, item);
		if (finput.state != null)
			throw new SuspendProgramException(finput.state);
		
		var facility = shrtctx.facilities.get(item.facility);
		
		var functioncfg = getFunctionConfig(
				runp.rtdatactx,
				item.facility,
				item.function);
		
		runp.shsession.rt = this;
		runp.shsession.datactx = runp.datactx;
		runp.shsession.input = finput.input;
		runp.shsession.outputtype = functioncfg.getst("output");
		runp.shsession.blockid = null;

		var cbmode = functioncfg.geti("block_mode");
		switch (cbmode) {
		case 1:
			runp.shsession.blockid = getBlockId(item);
			codeblock = runp.shsession.codeblock(runp.shsession.blockid);
			codeblock.index = item.index;
			codeblock.leave = false;
			runp.shsession.blockstack.push(runp.shsession.blockid);
			break;
		case -1:
			runp.shsession.blockid = runp.shsession.blockstack.pop();
			codeblock = runp.shsession.codeblock(runp.shsession.blockid);
			item.reference = codeblock.index;
			break;
		default:
			codeblock = null;
		}
		
		var object = facility.run(item.function, runp.shsession);
		if ((cbmode == 2) && !codeblock.leave)
			throw new GotoException(codeblock.index + 1, object);
		return object;
	}
	
	public final DataObject run(
			DBSession dbsession,
			CauldronProgram program,
			Map<String, CauldronProgram> programs,
			DataContext datactx) throws Exception {
		
		var runp = new RunParameters();
		runp.datactx = new DataContext();
		runp.programs = programs;
		runp.rtdatactx = rtctx.datactx;
		runp.shsession.dbsession = dbsession;
		
		runp.input = new HashMap<>();
		for (String key : datactx.entries())
			runp.input.put(key, datactx.get(key));
		
		return run(runp, program);
	}
	
	public final DataObject run(
			DBSession dbsession,
			CauldronProgram program,
			Map<String, DataObject> input) throws Exception {
		
		var runp = new RunParameters();
		runp.datactx = new DataContext();
		runp.input = new HashMap<>(input);
		runp.rtdatactx = rtctx.datactx;
		runp.shsession.dbsession = dbsession;
		
		return run(runp, program);
	}
	
	private final DataObject run(
			RunParameters runp,
			CauldronProgram program) throws Exception {
		DataObject document = null;
		ProgramItem item = null;
		
		for (int i = program.context.items.length - 1; i >= 0; i--)
			try {
				item = program.context.items[i];
				document = run(runp, program, item);
			} catch (GotoException e) {
				i = e.index;
				document = e.object;
			} finally {
				if (document == null)
					continue;
				runp.input.put(item.output, document);
				var reference = runp.references.get(item.reference);
				if (reference == null) {
					reference = new HashMap<>();
					runp.references.put(item.reference, reference);
				}
				
				reference.put(item.source, item.output);
			}
		
		return document;
	}
	
	public final void setConnector(ContextEntry ctxentry) {
		ctxentry.set(connector);
	}
	
	public final void useDummyConnector() {
		usedummyconn = true;
	}
	
	private final String validateFunctionConfig(
			String function,
			DataObject functioncfg) {
		
		var finput = functioncfg.getItem("input");
		var typectx = rtctx.typectx();
		
		for (var input : finput.getItems()) {
			var type = finput.getItem(input).getst("type");
			if (typectx.get(type) != null)
				continue;
			
			return String.format(
				"invalid type '%s' for parameter '%s' at function '%s'.",
				type, input, function);
		}
		
		var type = functioncfg.getst("output");
		if (typectx.get(type) == null)
			return String.format(
				"invalid output type '%s' at function '%s'.", type, function);
		
		return null;
	}
}

class FunctionInput {
	public Map<String, DataObject> input;
	public ProgramState state;
	
	public FunctionInput() {
		input = new HashMap<>();
	}
}

class RunParameters {
	public DataContext datactx, rtdatactx;
	public Map<String, DataObject> input;
	public SharedSession shsession;
	public Map<Integer, Map<String, String>> references;
	public Map<String, CauldronProgram> programs;
	
	public RunParameters() {
		shsession = new SharedSession();
		references = new HashMap<>();
	}
}

class GotoException extends Exception {
	private static final long serialVersionUID = -5865939765779242050L;
	public int index;
	public DataObject object;
	
	public GotoException(int index, DataObject object) {
		this.index = index;
		this.object = object;
	}
}
