package org.quanticsoftware.automata.runtime;

public interface DatabaseRule {

	public abstract void execute(DatabaseRuleSession session);
	
}
