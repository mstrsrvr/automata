package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.application.DataObject;

public interface Function {

	public abstract void beginblock();
	
	public abstract void config(DataObject functioncfg);
	
	public abstract void endblock();
	
	public abstract void input(String type, String name);
	
	public abstract void output(String type);
	
	public abstract void passthrough();
	
	public abstract void rule(FunctionRule rule);
	
	public abstract DataObject run(SharedSession shsession);
}

