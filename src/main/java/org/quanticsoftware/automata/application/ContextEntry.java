package org.quanticsoftware.automata.application;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.cauldron.ProgramState;
import org.quanticsoftware.automata.runtime.connector.Connector;
import org.quanticsoftware.automata.runtime.connector.DBSession;

public class ContextEntry {
	private String page;
	private Stack<String> pagestack;
	private DataContext datactx;
	public Cauldron cauldron;
	public Map<String, DataObject> objects;
	public Map<String, Set<String>> references;
	public Set<String> vtargets, inputs;
	public String output, action, updtarea, msgstatus, msgtext, otarget;
	public String indexpage;
	public Object[] msgargs;
	public ProgramState suspendstate;
	public boolean reload, fail;
	public Exception ex;
	public Locale locale;
	public DBSession dbsession;
	
	public ContextEntry() {
		vtargets = new HashSet<>();
		objects = new HashMap<>();
		references = new HashMap<>();
		cauldron = new Cauldron();
		inputs = new HashSet<>();
		pagestack = new Stack<>();
		datactx = new DataContext();
	}
	
	public final void add(DataObject object) {
		var name = object.getName();
		objects.put(name, object);
		var type = object.getType();
		var typepath = type.getPath();
		var objnames = references.get(typepath);
		if (objnames == null)
			references.put(typepath, objnames = new HashSet<>());
		objnames.add(name);
		for (var key : object.getItems()) {
			var item = object.getItem(key);
			/*
			 * we must consider that an item may be defined by reference and
			 * not referenced, thus, correctly yielding null. we must cover
			 * that feature.
			 */
			if (item != null)
				add(item);
		}
	}
	
	public final void alias(String alias, DataObject object) {
		objects.put(alias, object);
		var type = object.getType().getPath();
		var objnames = references.get(type);
		if (objnames == null)
			references.put(type, objnames = new HashSet<>());
		objnames.add(alias);
		for (var key : object.getItems()) {
			var cname = new StringBuilder(alias).append(".").append(key);
			var item = object.getItem(key);
			if (item != null)
				alias(cname.toString(), item);
		}
	}
	
	public final String getPage() {
		return page;
	}
	
	public final DataObject instance(DataType type) {
		var object = datactx.instance(type);
		add(object);
		return object;
	}
	
	public final DataObject instance(DataType type, String name) {
		var object = datactx.instance(type, name);
		add(object);
		return object;
	}
	
	public final String popPage() {
		return page = pagestack.pop();
	}
	
	public final void pushPage(String page) {
		if (this.page == null) {
			this.page = page;
			return;
		}
		
		if (page.equals(this.page))
			return;
		pagestack.push(this.page);
		this.page = page;
	}
	
	public final void set(Connector connector) {
		dbsession = connector.instance(datactx);
	}
}
