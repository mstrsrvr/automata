package org.quanticsoftware.automata.application;

import java.util.Map;

public class GetString {
    public static final String run(Map<String, ?> values, String name) {
        try {
            return (String)values.get(name);
        } catch (ClassCastException e) {
            return ((String[])values.get(name))[0];
        }
    }
}
