package org.quanticsoftware.automata.application;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class DataType {
	private String name, key, path, supertype;
	private DataType parent, element;
	private Map<String, TypeDetail> items;
	private Map<String, String> aliases;
	private boolean autogenkey;
	private SharedTypeContext shtypectx;
	private int colltype;
	
	public DataType(SharedTypeContext shtypectx,
			DataType parent, String name, String key) {
		this(shtypectx, parent, null, name, key);
	}
	
	public DataType(SharedTypeContext shtypectx,
			DataType parent, String supertype, String name, String key) {
		this.name = name;
		this.shtypectx = shtypectx;
		this.parent = parent;
		this.supertype = supertype;
		this.key = key;
		aliases = new HashMap<>();
		items = new LinkedHashMap<>();
		path = getAbsoluteName(parent, name);
	}
	
	private final DataType add(
			DataType extend, String name, boolean allocate) {
		var absolute = getAbsoluteName(this, name);
		var supertype = extend.getSuperType();
		var stname = (supertype == null)?extend.getName() : supertype.getName();
		var type = new DataType(shtypectx, this, stname, name, extend.getKey());
		type.copy(extend);
		items.put(name, new TypeDetail(absolute, allocate));
		shtypectx.types.put(absolute, type);
		alias(type, name);
		return type;
	}
	
	public final DataType add(DataType type, String name) {
		return add(type, name, true);
	}
	
	public final DataType addbl(String name) {
		return add(shtypectx.types.get("boolean"), name, true);
	}
	
	public final DataType addd(String name) {
		return add(shtypectx.types.get("double"), name, true);
	}
	
	public final DataType adddt(String name) {
		return add(shtypectx.types.get("date"), name, true);
	}
	
	public final DataType addi(String name) {
		return add(shtypectx.types.get("int"), name, true);
	}
	
	public final DataType addl(String name) {
		return add(shtypectx.types.get("long"), name, true);
	}
	
//	public final DataType addn(String name) {
//		return add(shtypectx.types.get("numop"), name, true);
//	}
	
	public final DataType addst(String name) {
		return add(shtypectx.types.get("string"), name, true);
	}
	
	public final DataType addtm(String name) {
		return add(shtypectx.types.get("time"), name, true);
	}

	private final void alias(DataType type, String name) {
		aliases.put(name, type.getPath());
		for (var key : type.getItems())
			alias(type.get(key), name.concat(".").concat(key));
	}
	
	public final void autoGenKey() {
		if (key == null)
			throw new GeneralException(
					"auto-generated key for %s requires key definition.", name);
		autogenkey = true;
	}
	
	public final void copy(DataType type) {
		setCollection(type.getCollectionType(), type.getElement());
		for (String key : type.getItems())
			add(type.get(key), key, true);
	}
	
	@Override
	public final boolean equals(Object type) {
		if (type == null)
			return false;
		if (type == this)
			return true;
		return toString().equals(type.toString());
	}
	
	public final DataType get(String name) {
		var item = items.get(name);
		if (item != null)
			return shtypectx.types.get(item.absolute);
		return shtypectx.types.get(aliases.get(name));
	}
	
	private final String getAbsoluteName(DataType parent, String name) {
		DataType type;
		if (parent == null)
		    return name;
		var sb = new StringBuilder();
		if ((type = parent.getParent()) != null)
		    sb.append(getAbsoluteName(type, parent.getName()));
		else
		    sb.append(parent.getName());
		return sb.append(".").append(name).toString();
	}
	
	public final int getCollectionType() {
		return colltype;
	}
	
	public final DataType getElement() {
		return element;
	}
	
	public final Set<String> getItems() {
		return items.keySet();
	}
	
	public final String getKey() {
		return key;
	}
	
	public final String getName() {
		return name;
	}
	
	public final DataType getParent() {
		return parent;
	}
	
	public final String getPath() {
	    return path;
	}
	
	public final DataType getSuperType() {
		return (supertype == null)? null : shtypectx.types.get(supertype);
	}
	
	public final boolean isAllocable(String name) {
		return items.get(name).allocate;
	}
	
	public final boolean isCollection() {
		return element != null;
	}
	
	public final boolean isAutoGenKey() {
		return autogenkey;
	}
	
	public final boolean isCompatible(String path) {
		if (this.path.equals(path))
			return true;
		if (supertype == null)
			return false;
		return shtypectx.types.get(supertype).isCompatible(path);
	}
	
	public final DataType ref(DataType type, String name) {
		return add(type, name, false);
	}
	
	public final void setCollection(int colltype, DataType type) {
		this.colltype = colltype;
		this.element = type;
	}
	
	@Override
	public final String toString() {
        int i = 0;
        var sb = new StringBuilder(name);
        if (element != null)
        	sb.append(":").append(element.getName()).append("[]");
    	sb.append(" {");
        for (String key : items.keySet()) {
            if (i++ > 0)
                sb.append(", ");
            var item = items.get(key);
            sb.append(key).append(":").
            	append(shtypectx.types.get(item.absolute).getSuperType());
        }
        return sb.append("}").toString();
	}
}

class TypeDetail {
	public String absolute;
	public boolean allocate;
	
	public TypeDetail(String absolute, boolean allocate) {
		this.absolute = absolute;
		this.allocate = allocate;
	}
}
