package org.quanticsoftware.automata.application;

public class ErrorMessageException extends GeneralException {
	private static final long serialVersionUID = 8552157804182530791L;
	public String text;
	public Object[] args;
	
	public ErrorMessageException(String text, Object[] args) {
		super(text, args);
		this.text = text;
		this.args = args;
	}
	
}
