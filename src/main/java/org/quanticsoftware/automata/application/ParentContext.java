package org.quanticsoftware.automata.application;

public class ParentContext {
	public DataObject object;
	public DataObjectAction action;
	
	public ParentContext(DataObject object) {
		this.object = object;
	}
}
