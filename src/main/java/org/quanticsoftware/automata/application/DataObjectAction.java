package org.quanticsoftware.automata.application;

public interface DataObjectAction {
	
	public abstract void execute(String action);
	
}
