package org.quanticsoftware.automata.application;


public class GetLeastType {
	
	public static final DataType get(DataType type) {
		while (type != null) {
			var stype = type.getSuperType();
			if (stype == null)
				break;
			type = stype;
		}
		return type;
	}
}
