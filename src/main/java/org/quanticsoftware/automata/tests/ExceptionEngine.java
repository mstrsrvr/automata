package org.quanticsoftware.automata.tests;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;
import org.quanticsoftware.automata.output.engines.AbstractRenderEngine;

public class ExceptionEngine extends AbstractRenderEngine {

	public ExceptionEngine() {
		super("pre");
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		switch (renderctx.element.name) {
		case "title":
			output.addInner(renderctx.ex.getMessage());
			break;
		case "stacktrace":
			for (var traceitem : renderctx.ex.getStackTrace())
				output.addInner(traceitem.toString().concat("\n"));
			break;
		}
	}
	
}