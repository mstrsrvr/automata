package org.quanticsoftware.automata.tests.logistics;

import javax.servlet.annotation.WebServlet;

import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.servlet.AbstractApplicationServlet;
import org.quanticsoftware.automata.tests.SystemFacility;

@WebServlet(urlPatterns = {"/logistics", "/logistics/index.html"})
public class LogisticsApplicationServlet extends AbstractApplicationServlet {
	private static final long serialVersionUID = -7230413328658494298L;

	@Override
	protected String init(RuntimeContext context) {
		SystemFacility.define(context);
		
//		var purchase_req = typectx.define("purchase_request", "id");
//		purchase_req.addi("id");
//		purchase_req.autoGenKey();
//		
//		var purchase_order = typectx.define("purchase_order", "id");
//		purchase_order.addi("id");
//		purchase_order.ref(purchase_req, "pr");
//		purchase_order.autoGenKey();
//		
//		var cancelling_req = typectx.define("cancelling_request", "id");
//		cancelling_req.addi("id");
//		cancelling_req.ref(purchase_order, "po");
//		cancelling_req.autoGenKey();
//		
//		var sales_order = typectx.define("sales_order", "id");
//		sales_order.addi("id");
//		sales_order.ref(purchase_order, "po");
//		sales_order.autoGenKey();
//		
//		var delivery_note = typectx.define("delivery_note", "id");
//		delivery_note.addi("id");
//		delivery_note.ref(sales_order, "so");
//		delivery_note.autoGenKey();
//		
//		var billing_document = typectx.define("billing_document", "id");
//		billing_document.addi("id");
//		billing_document.ref(delivery_note, "dn");
//		billing_document.autoGenKey();
//		
//		var material_movement = typectx.define("material_movement", "id");
//		material_movement.addi("id");
//		material_movement.ref(material_movement, "og");
//		material_movement.ref(purchase_order, "po");
//		material_movement.ref(delivery_note, "dn");
//		material_movement.autoGenKey();
//		
//		var facility = context.facility("mm");
//		var function = facility.function("purchase_request");
//		function.output("purchase_request");
//		function.rule((s)->{ });
//		
//		function = facility.function("purchase_order_create");
//		function.input("purchase_request", "pr");
//		function.output("purchase_order");
//		function.rule((s)->{
//			s.output().set("pr", s.input.get("pr"));
//		});
//		
//		function = facility.function("incoming_goods_post");
//		function.input("material_movement", "og");
//		function.input("purchase_order", "po");
//		function.output("material_movement");
//		function.rule((s)->{
//			var output = s.output();
//			output.set("og", s.input.get("og"));
//			output.set("po", s.input.get("po"));
//		});
//		
//		facility = context.facility("sd");
//		function = facility.function("sales_order_create");
//		function.input("purchase_order", "po");
//		function.output("sales_order");
//		function.rule((s)->{
//			s.output().set("po", s.input.get("po"));
//		});
//		
//		function = facility.function("delivery_note_post");
//		function.input("sales_order", "so");
//		function.output("delivery_note");
//		function.rule((s)->{
//			s.output().set("so", s.input.get("so"));
//		});
//		
//		function = facility.function("outgoing_goods_post");
//		function.input("delivery_note", "dn");
//		function.output("material_movement");
//		function.rule((s)->{
//			s.output().set("dn", s.input.get("dn"));
//		});
//		
//		function = facility.function("billing_post");
//		function.input("delivery_note", "dn");
//		function.output("billing_document");
//		function.rule((s)->{
//			s.output().set("dn", s.input.get("dn"));
//		});
//		
//		var goods_delivered = context.target("goods_delivered");
//		goods_delivered.input(purchase_req, "purchase_req_01");
//		goods_delivered.returns = "material_movement";

//		var purchase_cancel_ack = context.target("purchase_cancel_ack");
//		purchase_cancel_ack.input(purchase_order, "purchase_order_01");
//		purchase_cancel_ack.cancels = "goods_delivered";
		
//		var billing_issued = context.target("billing_issued");
//		billing_issued.input(purchase_order, "purchase_order_01");
//		billing_issued.returns = "billing_document";
		
//		var cancelling_issued = context.target("cancelling_issued");
//		cancelling_issued.input(cancelling_req, "cancelling_req_01");
//		cancelling_issued.cancels = "billilng_issued";
		
//		var customer = context.entity("customer");
//		customer.add(goods_delivered);
//		customer.add(purchase_cancel_ack);
//		
//		var vendor = context.entity("vendor");
//		vendor.add(billing_issued);
//		vendor.add(cancelling_issued);
		
//		var page = context.page("object_display");
//		page.spec = (p)->{
//			context.pagespectemplate.execute(p);
//			p.tree("main", "output_object");
//		};
//		
//		page.config = (p)->{
//			context.pageconfigtemplate.execute(p);
//			p.get("output_object").datatype = "material_movement";
//		};
//		
//		return "goods_delivered";

		var typectx = context.typectx();
		
		var op = 5;
		var double_t = typectx.get("double");
		
		var item_t = typectx.define("item_t");
		item_t.addd("quantity");
		item_t.addd("unitary_price");
		item_t.addd("total_price");
		
		var combine_t = typectx.define("combine_t");
		combine_t.add(item_t, "item_a");
		combine_t.add(item_t, "item_b");
		
		var items_t = typectx.array(item_t, "items_t");
		
		switch (op) {
//		test1
		case 0:
			var target = context.target(double_t, "mul");
			target.input(double_t, "i_a");
			target.input(double_t, "i_b");
			
			var test = target.test();
			test.input("i_a").set((double)2);
			test.input("i_b").set((double)2);
			test.rule = (t,o)->o.getd() == 4;
			
			test = target.test();
			test.input("i_a").set((double)2);
			test.input("i_b").set((double)3);
			test.rule = (t,o)->o.getd() == 6;
			
			SystemFacility.pagegenerate(context, "double");
			break;
			
//		test2
		case 1:
			target = context.target(double_t, "mul");
			target.input(item_t, "item");
			
			test = target.test();
			var item = test.input("item");
			item.set("quantity", (double)2);
			item.set("unitary_price", (double)3);
			test.rule = (t,o)->o.getd() == 6;
			test = target.test();
			
			item = test.input("item");
			item.set("quantity", (double)3);
			item.set("unitary_price", (double)3);
			test.rule = (t,o)->o.getd() == 9;
			
			SystemFacility.pagegenerate(context, "double");
			break;
			
//		test3
		case 2:
			target = context.target(item_t, "mul");
			target.input(item_t, "item");
			
			test = target.test();
			item = test.input("item");
			item.set("quantity", (double)1);
			item.set("unitary_price", (double)1);
			test.instance("quantity").rule = (t,o)->o.getd() == 1;
			test.instance("unitary_price").rule = (t,o)->o.getd() == 1;
			test.instance("total_price").rule = (t,o)->o.getd() == 1;
			
			test = target.test();
			item = test.input("item");
			item.set("quantity", (double)2);
			item.set("unitary_price", (double)7);
			test.instance("quantity").rule = (t,o)->o.getd() == 2;
			test.instance("unitary_price").rule = (t,o)->o.getd() == 7;
			test.instance("total_price").rule = (t,o)->o.getd() == 14;
			
			SystemFacility.pagegenerate(context, "item_t");
			break;

//		test4
		case 3:
			target = context.target(combine_t, "mul");
			target.input(combine_t, "combine");
			
			test = target.test();
			var combine = test.input("combine");
			combine.getItem("item_a").set("quantity", (double)2);
			combine.getItem("item_a").set("unitary_price", (double)3);
			test.instance("item_a.quantity").rule = (t,o)->o.getd() == 2;
			test.instance("item_a.unitary_price").rule = (t,o)->o.getd() == 3;
			test.instance("item_a.total_price").rule = (t,o)->o.getd() == 6;
			
			test = target.test();
			combine = test.input("combine");
			combine.getItem("item_a").set("quantity", (double)5);
			combine.getItem("item_a").set("unitary_price", (double)7);
			test.instance("item_a.quantity").rule = (t,o)->o.getd() == 5;
			test.instance("item_a.unitary_price").rule = (t,o)->o.getd() == 7;
			test.instance("item_a.total_price").rule = (t,o)->o.getd() == 35;
			
			SystemFacility.pagegenerate(context, "combine_t");
			break;
			
//		test5
		case 4:
			target = context.target(combine_t, "mul");
			target.input(combine_t, "combine");
			
			test = target.test();
			combine = test.input("combine");
			combine.getItem("item_a").set("quantity", (double)2);
			combine.getItem("item_a").set("unitary_price", (double)3);
			combine.getItem("item_b").set("quantity", (double)5);
			combine.getItem("item_b").set("unitary_price", (double)5);
			test.instance("item_a.quantity").rule = (t,o)->o.getd() == 2;
			test.instance("item_a.unitary_price").rule = (t,o)->o.getd() == 3;
			test.instance("item_a.total_price").rule = (t,o)->o.getd() == 6;
			test.instance("item_b.quantity").rule = (t,o)->o.getd() == 5;
			test.instance("item_b.unitary_price").rule = (t,o)->o.getd() == 5;
			test.instance("item_b.total_price").rule = (t,o)->o.getd() == 25;

			test = target.test();
			combine = test.input("combine");
			combine.getItem("item_a").set("quantity", (double)7);
			combine.getItem("item_a").set("unitary_price", (double)10);
			combine.getItem("item_b").set("quantity", (double)11);
			combine.getItem("item_b").set("unitary_price", (double)2);
			test.instance("item_a.quantity").rule = (t,o)->o.getd() == 7;
			test.instance("item_a.unitary_price").rule = (t,o)->o.getd() == 10;
			test.instance("item_a.total_price").rule = (t,o)->o.getd() == 70;
			test.instance("item_b.quantity").rule = (t,o)->o.getd() == 11;
			test.instance("item_b.unitary_price").rule = (t,o)->o.getd() == 2;
			test.instance("item_b.total_price").rule = (t,o)->o.getd() == 22;
			
			SystemFacility.pagegenerate(context, "combine_t");
			break;
		case 5:
			var data = new double[2][3];
			data[0][0] = 2;
			data[0][1] = 3;
			data[0][2] = 6;
			data[1][0] = 5;
			data[1][1] = 5;
			data[1][2] = 25;
			
			target = context.target(items_t, "mul");
			target.input(items_t, "items");
			
			test = target.test();
			var items = test.input("items");
			item = items.instance();
			item.set("quantity", data[0][0]);
			item.set("unitary_price", data[0][1]);
			test.instance("total_price").rule = (t,o)->{
				return o.getd() == data[0][2];
			};

			test = target.test();
			items = test.input("items");
			item = items.instance();
			item.set("quantity", data[1][0]);
			item.set("unitary_price", data[1][1]);
			test.instance("total_price").rule = (t,o)->{
				return o.getd() == data[1][2];
			};
			
			SystemFacility.pagegenerate(context, "items_t");
		}
		
		return "catalog";
	}

	
}
