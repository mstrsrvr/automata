package org.quanticsoftware.automata.tests;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;
import org.quanticsoftware.automata.output.engines.AbstractRenderEngine;

public class MessageEngine extends AbstractRenderEngine {

	public MessageEngine() {
		super("p");
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		if (renderctx.msgtext == null)
			return;
		var text1 = toString(renderctx, renderctx.msgtext, renderctx.msgargs);
		var text2 = toString(renderctx, renderctx.msgstatus);
		output.addInner(text2.concat(": ").concat(text1));
	}
	
}
