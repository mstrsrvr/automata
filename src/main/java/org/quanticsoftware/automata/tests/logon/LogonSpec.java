package org.quanticsoftware.automata.tests.logon;

import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.tests.BasePageSpec;

public class LogonSpec extends BasePageSpec {
	
	@Override
	public final void execute(PageContext pagectx) {
		super.execute(pagectx);
		pagectx.dataform("main", "logon_input");
		pagectx.textfield("logon_input", "logon_input.password");
		pagectx.text("logon_input", "logon_input.username_text");
		pagectx.text("logon_input", "logon_input.password_text");
		pagectx.button("main", "logon");
		pagectx.text("main", "message");
	}
}
