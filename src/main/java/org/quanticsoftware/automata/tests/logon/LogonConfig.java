package org.quanticsoftware.automata.tests.logon;

import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.tests.BasePageConfig;

public class LogonConfig extends BasePageConfig {
	
	@Override
	public final void execute(PageContext pagectx) {
		super.execute(pagectx);
		pagectx.get("logon_input").datatype = "logon";
		pagectx.get("logon_input.password").secret = true;
		var logonbt = pagectx.get("logon");
		logonbt.submit = true;
		logonbt.action = "terminal.run";
		
		pagectx.get("message").engine = "message";
		
		var messages = pagectx.getMessages("pt_BR");
		messages.put("error", "Erro");
		messages.put(
				"invalid.username.password", "Usuário ou senha inválidos");
		messages.put("logon_input.username_text", "Usuário");
		messages.put("logon_input.password_text", "Senha");
	}
}
