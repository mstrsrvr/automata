package org.quanticsoftware.automata.tests.exhandler;

import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.tests.BasePageSpec;

public class ExHandlerSpec extends BasePageSpec {
	
	@Override
	public final void execute(PageContext pagectx) {
		super.execute(pagectx);
		pagectx.text("main", "title");
		pagectx.text("main", "stacktrace");
		pagectx.text("main", "message");
	}
}
