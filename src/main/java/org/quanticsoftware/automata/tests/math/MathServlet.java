package org.quanticsoftware.automata.tests.math;

import javax.servlet.annotation.WebServlet;

import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.servlet.AbstractApplicationServlet;
import org.quanticsoftware.automata.tests.SystemFacility;

@WebServlet(urlPatterns = {"/math", "/math/index.html"})
public class MathServlet extends AbstractApplicationServlet {
	private static final long serialVersionUID = -5300200370989991248L;

	@Override
	protected String init(RuntimeContext context) {
		SystemFacility.define(context);
		
		var double_t = context.typectx().get("double");
		
		var facility = context.facility("math");
		var function = facility.function("add");
		function.input("double", "a");
		function.input("double", "b");
		function.output("double");
		function.rule((s)->s.output().set(s.getd("a") + s.getd("b")));
		
		function = facility.function("mul");
		function.input("double", "a");
		function.input("double", "b");
		function.output("double");
		function.rule((s)->s.output().set(s.getd("a") * s.getd("b")));
		
		var target = context.target(double_t, "math_all_add");
		target.input(double_t, "op_a");
		target.input(double_t, "op_b");
		target.input(double_t, "op_c");
		
		var test = target.test();
		test.input("op_a").set((double)1);
		test.input("op_b").set((double)1);
		test.input("op_c").set((double)1);
		test.rule = (t,o)->o.getd() == 3;
		
		target = context.target(double_t, "math_all_mul");
		target.input(double_t, "op_a");
		target.input(double_t, "op_b");
		target.input(double_t, "op_c");
		
		test = target.test();
		test.input("op_a").set((double)5);
		test.input("op_b").set((double)2);
		test.input("op_c").set((double)3);
		test.rule = (t,o)->o.getd() == 30;

		target = context.target(double_t, "math_add_mul");
		target.input(double_t, "op_a");
		target.input(double_t, "op_b");
		target.input(double_t, "op_c");
		
		test = target.test();
		test.input("op_a").set((double)2);
		test.input("op_b").set((double)3);
		test.input("op_c").set((double)1);
		test.rule = (t,o)->o.getd() == 7;

		SystemFacility.pagegenerate(context, "double");
		SystemFacility.pagegenerate(context, "context");
		
		return "catalog";
	}

}
