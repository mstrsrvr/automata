package org.quanticsoftware.automata.servlet;

import org.quanticsoftware.automata.application.ContextEntry;

public interface DataConverter {
	
	public abstract Object run(ContextEntry ctxentry, String value)
			throws Exception;
}
