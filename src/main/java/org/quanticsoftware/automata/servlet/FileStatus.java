package org.quanticsoftware.automata.servlet;

import org.apache.commons.fileupload.FileItem;

public class FileStatus {
    public FileItem item;
    public boolean file;
    
    public FileStatus(FileItem item) {
        this.item = item;
    }
}
