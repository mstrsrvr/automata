package org.quanticsoftware.automata.servlet;

import org.quanticsoftware.automata.application.ContextEntry;
import org.quanticsoftware.automata.application.GeneralException;
import org.quanticsoftware.automata.cauldron.ProgramState;
import org.quanticsoftware.automata.runtime.EventHandler;
import org.quanticsoftware.automata.runtime.PageType;
import org.quanticsoftware.automata.runtime.Runtime;

public class MissingInput implements EventHandler {
	private Runtime runtime;
	
	public MissingInput(Runtime runtime) {
		this.runtime = runtime;
	}

	private final PageType generatePage(
			ContextEntry ctxentry, ProgramState state) {
		var otarget = ctxentry.otarget;
		var rtctx = runtime.context();
		var page = rtctx.page(state.finput);
		
		var pagetype = runtime.shrtctx.pagetype(
				state.finput,
				state.ftype,
				state.finput,
				true);
		
		var engine = pagetype.engine;
		
		page.spec = (p)->{
			if (rtctx.pagespectemplate != null) {
				rtctx.pagespectemplate.execute(p);
			} else {
				p.view();
				p.form("main");
			}
			
			var type = rtctx.typectx().get(state.ftype);
			if (!type.isCollection()) {
				if (type.getItems().size() == 0) {
					var item = p.container(
							"main",
							state.finput.concat("_item"));
					
					p.text(item.name, state.finput.concat("_text"));
					p.textfield(item.name, state.finput);
				} else {
					p.dataform("main", state.finput);
				}
			}
			
			p.button("main", "go");
		};
		
		page.config = (p)->{
			if (rtctx.pageconfigtemplate != null)
				rtctx.pageconfigtemplate.execute(p);
			
			var element = p.get(state.finput);
			element.datatype = state.ftype;
			element.engine = engine;
			p.get("go").action = otarget;
		};

		return pagetype;
	}
	
	@Override
	public final void run(ContextEntry ctxentry, Object value) {
		var state = (ProgramState)value;
		var pagetypes = runtime.shrtctx.pagetypes.get(state.ftype);
		
		var pagetype = (pagetypes != null)? pagetypes.get(state.finput) : null;
		if ((pagetype == null) || pagetype.isAutomatic())
			pagetype = generatePage(ctxentry, state);
		
		if (pagetype == null)
			throw new GeneralException(
					"no page for output type '%s'.", state.ftype);
		
		state.output = pagetype.element();
		ctxentry.pushPage(pagetype.page());
	}
}
